import biuoop.GUI;


import game.graphics.Animation;
import game.graphics.AnimationRunner;
import game.graphics.HighScoresAnimation;
import game.graphics.KeyPressStoppableAnimation;
import game.graphics.ShowHiScoresTask;
import game.graphics.Task;
import game.graphics.Menu;
import game.graphics.MenuAnimation;
import game.logic.HighScoresTable;
import game.logic.GameFlow;

import java.io.File;

/**
 * The Class represent the main of the game.
 * the class is responsible to run the game and include the main method of the whole program.
 *
 * @author Yaara Fridchay
 */
public class MainGame {

    private static final int WIDTH_SCREEN = 800;
    private static final int HEIGHT_SCREEN = 600;

    /**
     * The main method of the whole program.
     *
     * @param args - String array
     */
    public static void main(final String[] args) {
        File file = new File("highScores.txt");
        GUI gui = new GUI("Arkanoid OOP assignment 6", WIDTH_SCREEN, HEIGHT_SCREEN);
        biuoop.KeyboardSensor ks = gui.getKeyboardSensor();
        AnimationRunner ar = new AnimationRunner(gui, 60);
        HighScoresTable h = new HighScoresTable(10);
        Animation scoreT = new HighScoresAnimation(h);
        while (true) {
            //try to load the file table
            try {
                h.load(file);
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
            Animation scoresAnimation = new KeyPressStoppableAnimation(ks, ks.SPACE_KEY, scoreT);
            ShowHiScoresTask highScores = new ShowHiScoresTask(ar, scoresAnimation);
            // the quit task
            Task<Void> quit = new Task<Void>() {
                @Override
                public Void run() {
                    gui.close();
                    System.exit(0);
                    return null;
                }
            };
            // the subMenu task
            Task<Void> start = new Task<Void>() {
                @Override
                public Void run() {
                    GameFlow gameFlow = new GameFlow(ar, ks, gui);
                    gameFlow.runLevels();
                    return null;
                }
            };
            // add the selection to the menu.
            Menu<Task<Void>> menu = new MenuAnimation<Task<Void>>(ks, "Arakonid");
            menu.addSelection("s", "Start new game", start);
            menu.addSelection("h", "High scores", highScores);
            menu.addSelection("q", "quit", quit);
            ar.run(menu);
            // wait for user selection
            Task<Void> task = menu.getStatus();
            task.run();

        }
    }
}

