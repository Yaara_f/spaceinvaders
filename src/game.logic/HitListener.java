package game.logic;

import geometry.Ball;
import geometry.Block;
import geometry.Paddle;

/**
 * The method represent the interface HitListener.
 */
public interface HitListener {

    /**
     * This method is called whenever the beingHit object is hit.
     * The hitter parameter is the Ball that's doing the hitting.
     *
     * @param beingHit - Block
     * @param hitter   - Ball
     */
    void hitEvent(Block beingHit, Ball hitter);

    /**
     * Hit event.
     *
     * @param hitted the hitted
     * @param ball   the ball
     */
    void hitEvent(Paddle hitted, Ball ball);
}