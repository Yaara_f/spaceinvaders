package game.logic;

import biuoop.DialogManager;
import biuoop.GUI;
import biuoop.KeyboardSensor;
import game.graphics.AnimationRunner;
import game.graphics.KeyPressStoppableAnimation;
import game.graphics.LivesIndicator;
import game.graphics.ScoreIndicator;
import game.graphics.LoseMessage;
import game.graphics.WinningMessage;
import game.graphics.HighScoresAnimation;
import geometry.Block;
import geometry.Collidable;
import levels.LevelIndicator;
import levels.LevelInformation;
import levels.LevelSpecificationReader;

import java.io.File;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

/**
 * The program in charge of running all the levels in the game.
 */
public class GameFlow {
    private AnimationRunner animationRunner;
    private KeyboardSensor keyboardSensor;
    private GUI gui;
    private HighScoresTable h = new HighScoresTable(10);
    private File file = new File("highScores.txt");
    private int countRuns;

    /**
     * Constructor of GameFlow.
     *
     * @param ar  - AnimationRunner
     * @param ks  - KeyboardSensor
     * @param gui - GUI
     */
    public GameFlow(AnimationRunner ar, KeyboardSensor ks, GUI gui) {
        this.animationRunner = ar;
        this.keyboardSensor = ks;
        this.gui = gui;
        this.countRuns = 0;
        try {
            h.load(file);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    /**
     * The method run the levels of the game.
     */
    public void runLevels() {
        List<Block> blocks;
        GameLevel level;
        Counter counterScores = new Counter(0);
        ScoreTrackingListener score = new ScoreTrackingListener(counterScores);
        ScoreIndicator scores = new ScoreIndicator(counterScores);
        Counter numbersLives = new Counter(3);
        LivesIndicator lives = new LivesIndicator(numbersLives);
        LevelIndicator levelIndicator;
        SpriteCollection sprites = new SpriteCollection(new ArrayList<Sprite>());
        GameEnvironment environment = new GameEnvironment(new ArrayList<Collidable>());
        // if the user lose in the game.
        boolean lose = false;
        // run the levels of the game until the user lose.
        while (!lose) {
            // count which level
            this.countRuns++;
            // read the level from the game.
            String pathName = "space_invaders.txt";
            Reader read = new InputStreamReader(
                    ClassLoader.getSystemClassLoader().getResourceAsStream(pathName));
            LevelSpecificationReader levelSpecificationReader = new LevelSpecificationReader();
            List<LevelInformation> levelInfo = levelSpecificationReader.fromReader(read);
            //in this game there is only one level.
            blocks = levelInfo.get(0).blocks();
            // the name of the level.
            String nameLevel = levelInfo.get(0).levelName() + String.valueOf(countRuns);
            // create levelIndicator
            levelIndicator = new LevelIndicator(nameLevel);
            // the current level
            level = new GameLevel(sprites, environment, this.animationRunner, levelInfo.get(0),
                    this.keyboardSensor, counterScores, numbersLives);
            level.setBlocks(blocks);
            level.setLevelCount(countRuns);
            // initialize the current level
            level.initialize();
            // while remaining blocks and lives at the game.
            while (level.getCounterBlocks().getValue() > 0 && level.getNumbersLives().getValue() > 0
                    && !level.enemysWin()) {
                level.playOneTurn();
            }
            /*
                resetting the fields of the level.
             */
            gui = this.animationRunner.getGui();
            this.animationRunner = new AnimationRunner(gui, 60);
           /* sprites = new SpriteCollection(new ArrayList<Sprite>());
            environment = level.getEnvironment();*/
            blocks.clear();
            // if the user lose
            if (numbersLives.getValue() == 0) {
                lose = true;
            }
        }
        if (lose) {
            // run losing message
            this.animationRunner.run(new KeyPressStoppableAnimation(gui.getKeyboardSensor(),
                    KeyboardSensor.SPACE_KEY, new LoseMessage(counterScores)));
        }
        // if the user dosen't lose, print winning message.
        if (!lose) {
            this.animationRunner.run(new KeyPressStoppableAnimation(gui.getKeyboardSensor(),
                    KeyboardSensor.SPACE_KEY, new WinningMessage(counterScores)));
        }
        // if the player reach enough scores to be on the highScoreTable
        if (h.getRank(counterScores.getValue()) <= h.size()) {
            DialogManager dialog = gui.getDialogManager();
            String name = dialog.showQuestionDialog("Name", "What is your name?", "");
            h.add(new ScoreInfo(name, counterScores.getValue()));
            try {
                h.save(file);
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        }
        // run the highScore table.
        this.animationRunner.run(new KeyPressStoppableAnimation(gui.getKeyboardSensor(),
                KeyboardSensor.SPACE_KEY, new HighScoresAnimation(h)));
    }
}
