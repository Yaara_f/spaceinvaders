package game.logic;

import biuoop.DrawSurface;
import biuoop.KeyboardSensor;
import game.graphics.Animation;
import game.graphics.AnimationRunner;
import game.graphics.LivesIndicator;
import game.graphics.ScoreIndicator;
import game.graphics.CountdownAnimation;
import game.graphics.KeyPressStoppableAnimation;
import game.graphics.PauseScreen;
import geometry.BallRemover;
import geometry.Block;
import geometry.Ball;
import geometry.Collidable;
import geometry.Rectangle;
import geometry.Point;
import geometry.BlockRemover;
import geometry.Paddle;
import levels.ColorsParser;
import levels.LevelIndicator;
import levels.LevelInformation;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

/**
 * The Class represent the GameLevel that implement Animation interface.
 * GameLevel has some fields: the sprites of the game, the environment of the game and the GUI of the game
 * The program perform method that are related to Game,
 * like: create the blocks of the game, initialize the game and run the game .
 *
 * @author Yaara Fridchay
 */
public class GameLevel implements Animation {

    private static final int HEIGHT_PADDLE = 20;
    private static final int WIDTH_SCREEN = 800;
    private static final int HEIGHT_SCREEN = 600;
    private static final int SIZE_FRAME_BLOCKS = 30;
    private static final int SIZE_OF_BALL = 10;

    private List<Block> blocks;
    private List<Block> shields;
    private Counter counterBalls = new Counter(0);
    private BallRemover ballRemover = new BallRemover(this, counterBalls);
    //private PaddleRemover paddleRemover = new PaddleRemover(this);
    private Counter counterBlocks = new Counter(0);
    private Counter counterShields = new Counter(0);
    private BlockRemover blockRemover = new BlockRemover(this, counterBlocks);
    private BlockRemover shieldRemover = new BlockRemover(this, counterShields);
    private SpriteCollection sprites;
    private GameEnvironment environment;
    private Paddle p;
    private AnimationRunner runner;
    private boolean running;
    private LevelInformation levelInformation;
    private KeyboardSensor keyboardSensor;
    private ScoreTrackingListener score;
    private ScoreIndicator scores;
    private LivesIndicator lives;
    private LevelIndicator levelIndicator;
    private Counter numbersLives;
    private Counter counterScores;
    private int startNumBlocks;
    private List<Ball> shotEnemy = new ArrayList<>();
    private List<Ball> myShot = new ArrayList<>();
    private GroupEnemy enemys;
    private boolean paddleHitted = false;
    private List<Block> startedBlocks;
    private int levelCount;

    /**
     * Constructor of GameLevel.
     *
     * @param sprites          - the sprites of the game
     * @param environment      - the environment of the game
     * @param ar               -  AnimationRunner
     * @param levelInformation - LevelInformation
     * @param keyboardSensor   - Keyboard sensor
     * @param counterScores    - Counter of the scores
     * @param numbersLives     - Counter the lives
     */
    public GameLevel(SpriteCollection sprites, GameEnvironment environment, AnimationRunner ar,
                     LevelInformation levelInformation, KeyboardSensor keyboardSensor,
                     Counter counterScores, Counter numbersLives) {
        this.sprites = sprites;
        this.environment = environment;
        this.runner = ar;
        this.levelInformation = levelInformation;
        this.keyboardSensor = keyboardSensor;
        this.counterScores = counterScores;
        this.numbersLives = numbersLives;
        this.lives = new LivesIndicator(this.numbersLives);
        this.scores = new ScoreIndicator(this.counterScores);
        this.score = new ScoreTrackingListener(this.counterScores);
        this.blocks = new ArrayList<>();
        shields = new ArrayList<>();
        levelCount = 1;
    }

    /**
     * Sets level count.
     *
     * @param levelCounter the level count
     */
    public void setLevelCount(int levelCounter) {
        this.levelCount = levelCounter;
    }

    /**
     * Sets paddle hitted.
     *
     * @param b boolean
     */
    public void setPaddleHitted(boolean b) {
        this.paddleHitted = b;
    }

    /**
     * Remove block from list.
     *
     * @param block the block
     */
    public void removeBlockFromList(Block block) {
        this.blocks.remove(block);
    }

    /**
     * The method return the remain numbers of live.
     *
     * @return Counter. numbers lives
     */
    public Counter getNumbersLives() {
        return numbersLives;
    }

    /**
     * The method return the number of block that was in the beginning of the game.
     *
     * @return int. start num blocks
     */
    public int getStartNumBlocks() {
        return startNumBlocks;
    }

    /**
     * The method return the environment of the level.
     *
     * @return GameEnvironment environment
     */
    public GameEnvironment getEnvironment() {
        return environment;
    }

    /**
     * The method return the counter of the balls.
     *
     * @return Counter counter balls
     */
    public Counter getCounterBalls() {
        return this.counterBalls;
    }

    /**
     * The method return the counter of the blocks.
     *
     * @return Counter counter blocks
     */
    public Counter getCounterBlocks() {
        return this.counterBlocks;
    }

    /**
     * The method add collidable to the list of the collidables in the class GameEnvironment.
     *
     * @param c - collidable to add
     */
    public void addCollidable(Collidable c) {
        this.environment.addCollidable(c);
    }

    /**
     * Sets blocks.
     *
     * @param block the block
     */
    public void setBlocks(List<Block> block) {
        this.blocks = block;
    }

    /**
     * The method add sprite to the list of the sprites in the class SpriteCollection.
     *
     * @param s - sprite to add
     */
    public void addSprite(Sprite s) {
        this.sprites.addSprite(s);
    }

    /**
     * The method remove collidable from the list of the collidables in the class GameEnvironment.
     *
     * @param c - collidable to remove
     */
    public void removeCollidable(Collidable c) {
        this.environment.removeCollidable(c);
    }

    /**
     * The method remove sprite from the list of the sprites in the class SpriteCollection.
     *
     * @param s - sprite to remove
     */
    public void removeSprite(Sprite s) {
        this.sprites.removeSprite(s);
    }

    /**
     * The method create the balls of the level.
     */
    public void createBlocks() {
        for (int i = 0; i < this.blocks.size(); i++) {
            blocks.get(i).addHitListener(this.blockRemover);
            blocks.get(i).addHitListener(this.ballRemover);
            blocks.get(i).addHitListener(this.score);
            this.counterBlocks.increase(1);
            blocks.get(i).addToGame(this);
        }
        //the number of block that was in the beginning of the game.
        this.startNumBlocks = counterBlocks.getValue();
        enemys = new GroupEnemy(this.blocks, this, 50.0 * levelCount * 1.5);
    }

    /**
     * The method create the paddle of the game.
     */
    public void createPaddle() {
        this.paddleHitted = false;
        TreeMap<Integer, ColorsParser> fill = new TreeMap<>();
        fill.put(1, new ColorsParser(Color.orange));
        double widthP = this.levelInformation.paddleWidth();
        double heightP = HEIGHT_PADDLE;
        Point upperLeftP = new Point(380 - (widthP / 2), 600 - heightP);
        this.p = new Paddle(keyboardSensor, new Block(new Rectangle(upperLeftP, widthP, heightP),
                0, fill, Color.black, false), WIDTH_SCREEN,
                this.levelInformation.paddleSpeed(), Color.orange, this);

        this.p.addHitListener(this.ballRemover);
        this.p.addToGame(this);
    }

    /**
     * Reset shields.
     */
    public void resetShields() {
        for (int i = 0; i < this.shields.size(); i++) {
            shields.get(i).removeHitListener(this.shieldRemover);
            shields.get(i).removeHitListener(this.ballRemover);
            shields.get(i).removeFromGame(this);
        }

    }

    /**
     * Create shields.
     */
    public void createShields() {
        double width = 5;
        double height = 5;
        TreeMap<Integer, ColorsParser> fill = new TreeMap<>();
        fill.put(1, new ColorsParser(Color.cyan));
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                for (int k = 0; k < 30; k++) {
                    shields.add(new Block(new Rectangle(new Point(80 + k * 5 + i * 250, 500 + 5 * j),
                            width, height), 1, fill, Color.cyan, false));
                    counterShields.increase(1);
                }
            }
        }
        for (int i = 0; i < this.shields.size(); i++) {
            shields.get(i).addHitListener(this.shieldRemover);
            shields.get(i).addHitListener(this.ballRemover);
            shields.get(i).addToGame(this);
        }

    }

    /**
     * The method initialize a new level.
     */
    public void initialize() {
        removeBalls();
        addSprite(this.levelInformation.getBackground());
        runner = new AnimationRunner(this.runner.getGui(), 60);
        // the map include the number of fill and the fill.
        TreeMap<Integer, ColorsParser> fill = new TreeMap<>();
        fill.put(0, new ColorsParser(Color.black));
        //create the blocks of the frame
        Block up = new Block(new Rectangle(new Point(0, 0), WIDTH_SCREEN, SIZE_FRAME_BLOCKS),
                0, fill, Color.black, false);
        up.setText(up.getNumOfHit());
        Block right = new Block(new Rectangle(new Point(770, 0), SIZE_FRAME_BLOCKS, HEIGHT_SCREEN),
                0, fill, Color.black, false);
        right.setText(right.getNumOfHit());
        Block bottom = new Block(new Rectangle(new Point(0, 605), WIDTH_SCREEN, SIZE_FRAME_BLOCKS),
                0, fill, Color.black, false);
        bottom.addHitListener(this.ballRemover);
        bottom.setText(bottom.getNumOfHit());
        Block left = new Block(new Rectangle(new Point(0, 0), SIZE_FRAME_BLOCKS, HEIGHT_SCREEN),
                0, fill, Color.black, false);
        left.setText(left.getNumOfHit());
        bottom.addHitListener(this.ballRemover);
        right.addHitListener(this.ballRemover);
        left.addHitListener(this.ballRemover);
        up.addHitListener(this.ballRemover);
        // add to the game the paddle, the frame blocks and the ball.
        up.addToGame(this);
        right.addToGame(this);
        left.addToGame(this);
        //bottom.addToGame(this);
        this.scores.addToGame(this);
        this.levelIndicator = new LevelIndicator(this.levelInformation.levelName()
                + String.valueOf(levelCount));
        levelIndicator.addToGame(this);
        this.lives.addToGame(this);
        createBlocks();
        createShields();
    }

    /**
     * The method run the game = the animation loop.
     */
    public void playOneTurn() {
        createPaddle();
        this.runner.run(new CountdownAnimation(2, 3, this.sprites));
        this.running = true;
        this.runner.run(this);
        this.p.removeHitListener(this.blockRemover);
        this.p.removeFromGame(this);
        removeBalls();
    }

    /**
     * The method in charge of the logic of the level.
     *
     * @param d  - the drawSurface
     * @param dt - the amount of seconds passed since the last call.
     */
    public void doOneFrame(DrawSurface d, double dt) {
        enemys.setEnemys(this.blocks);
        this.sprites.drawAllOn(d);
        this.sprites.notifyAllTimePassed(dt);
        // if the user press "p" , pause the game.
        if (this.runner.getGui().getKeyboardSensor().isPressed("p")) {
            this.runner.run(new KeyPressStoppableAnimation(this.keyboardSensor,
                    KeyboardSensor.SPACE_KEY, new PauseScreen()));
        }
        this.enemys.timePassed(dt);

        // If there are no more blocks left or the player hit enough blocks to end the level.
        if (this.counterBlocks.getValue() <= 0) {
            // reset the level.
            for (int i = 0; i < this.blocks.size(); i++) {
                this.blocks.get(i).removeFromGame(this);
                this.blocks.get(i).removeHitListener(blockRemover);
                this.blocks.get(i).removeHitListener(score);
            }
            this.running = false;
            this.counterBlocks.decrease(this.counterBlocks.getValue());
            //destroying all blocks is worth another 100 points.
            this.counterScores.increase(100);
            return;
        }


    }

    /**
     * return if the method should stop.
     *
     * @return boolean
     */
    @Override
    public boolean shouldStop() {

        if (enemysWin() || this.paddleHitted) {
            this.numbersLives.decrease(1);
            this.enemys.reset();
            // p.removeFromGame(this);
            return true;
        }
        return !this.running;
    }

    /**
     * The method run the level.
     */
    public void run() {
        // while there are remaining blocks and lives, continue to run.
        while (this.numbersLives.getValue() > 0 && !enemysWin()) {
            playOneTurn();
        }
        this.runner.getGui().close();
    }


    /**
     * Enemy shot ball.
     *
     * @param point the point
     * @return the ball
     */
    public Ball enemyShot(Point point) {
        Ball shot = new Ball(point, 5, Color.red, this.environment, this, true);
        shot.addToGame(this);
        this.shotEnemy.add(shot);
        return shot;
    }

    /**
     * Player shot ball.
     *
     * @param point the point
     * @return the ball
     */
    public Ball playerShot(Point point) {
        Ball shot = new Ball(point, 6, Color.white, this.environment, this, false);
        this.myShot.add(shot);
        return shot;
    }

    /**
     * Enemys win boolean.
     *
     * @return the boolean
     */
    public boolean enemysWin() {
        boolean flag = false;
        for (int i = 0; i < this.enemys.getSizeGroup(); i++) {
            if (this.enemys.getEnamys().get(i).getCollisionRectangle().getUpperLeft().getY() == 480) {
                flag = true;
            }
        }
        return flag;
    }

    /**
     * Remove balls.
     */
    public void removeBalls() {
        for (int i = 0; i < this.myShot.size(); i++) {
            this.myShot.get(i).removeFromGame(this);
            removeSprite(this.myShot.get(i));
        }
        this.myShot.clear();
        for (int i = 0; i < this.shotEnemy.size(); i++) {
            this.shotEnemy.get(i).removeHitListener(this.ballRemover);
            this.shotEnemy.get(i).removeFromGame(this);
            removeSprite(this.shotEnemy.get(i));
        }
        this.shotEnemy.clear();
    }
}