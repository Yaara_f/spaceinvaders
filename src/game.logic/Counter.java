package game.logic;

/**
 * The Class represent Counter, any thing that need to be count.
 * Counter has one field: int that count.
 */
public class Counter {
    private int counter;

    /**
     * Constructor of Counter.
     *
     * @param c - int
     */
    public Counter(int c) {
        this.counter = c;
    }

    /**
     * The method add the given number to current count.
     *
     * @param number - the given number
     */
    public void increase(int number) {
        this.counter += number;
    }

    /**
     * The method subtract the given number from current count.
     *
     * @param number - the given number.
     */
    public void decrease(int number) {
        this.counter -= number;
    }

    /**
     * The method return the current count.
     *
     * @return int.
     */
    public int getValue() {
        return this.counter;
    }
}