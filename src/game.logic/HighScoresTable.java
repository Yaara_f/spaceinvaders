package game.logic;

import java.io.Serializable;
import java.io.File;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * The class represent the HighScoreTable.
 * HighScoreTable responsible to load the text file of the table, add players to the table, clear the table,
 * and return the highest score in the table.
 */
public class HighScoresTable implements Serializable {
    private ArrayList<ScoreInfo> highScores;
    private int size;

    /**
     * Constructor of HighScoreTable,the method Create an empty high-scores table with the specified size.
     *
     * @param size - size means that the table holds up to size top scores.
     */
    public HighScoresTable(int size) {
        this.size = size;
        this.highScores = new ArrayList<ScoreInfo>();
    }

    /**
     * The method add a high-score.
     *
     * @param score - the score to add.
     */
    public void add(ScoreInfo score) {
        int rank = this.getRank(score.getScore());
        // if the rank is in the size of the table.
        if (rank < this.size) {
            this.highScores.add(rank - 1, score);
        }
        // if the rank isn't in the size of the table.
        if (this.highScores.size() > this.size) {
            this.highScores.remove(this.size);
        }
    }

    /**
     * The method return the table size.
     *
     * @return int.
     */
    public int size() {
        return this.size;
    }

    /**
     * The method return the current high scores.
     * The list is sorted such that the highest scores come first.
     *
     * @return List<ScoreInfo>.
     */
    public List<ScoreInfo> getHighScores() {
        return this.highScores;
    }

    /**
     * The method return the rank of the current score: "where will i be on the list if added?"
     * Rank 1 means the score will be highest on the list.
     * Rank `size` means the score will be lowest.
     *
     * @param score the score the player reach.
     * @return int rank.
     */
    public int getRank(int score) {
        if (this.highScores.isEmpty()) {
            return 1;
        }
        // search the place of the score in the table,
        // Rank > `size` means the score is too low and will not be added to the list.
        for (int i = 0; i < this.highScores.size(); i++) {
            if (score > this.highScores.get(i).getScore()) {
                return i + 1;
            }
        }
        return this.highScores.size() + 1;
    }

    /**
     * The method clears the table.
     */
    public void clear() {
        this.highScores.clear();
    }

    /**
     * The method Load table data from file.
     *
     * @param filename - the file to read.
     * @throws IOException - exception.
     */
    public void load(File filename) throws IOException {
        // if the file is already exist, load it.
        if (filename.exists()) {
            if (loadFromFile(filename) != null) {
                this.highScores = loadFromFile(filename).highScores;
            } else {
                throw new IOException("Failed reading object");
            }
        }
    }

    /**
     * The method save table data to the specified file.
     *
     * @param filename - the file to read.
     * @throws IOException - exception.
     */
    public void save(File filename) throws IOException {
        ObjectOutputStream objectOutputStream = null;
        //try to save the table.
        try {
            objectOutputStream = new ObjectOutputStream(
                    new FileOutputStream(filename));
            objectOutputStream.writeObject(this);
        } catch (IOException e) {
            System.err.println("Failed saving object");
            e.printStackTrace(System.err);
        } finally {
            try {
                if (objectOutputStream != null) {
                    objectOutputStream.close();
                }
            } catch (IOException e) {
                System.err.println("Failed closing file: " + filename);
            }
        }
    }

    /**
     * The method read a table from file and return it.
     * If the file does not exist, or there is a problem with reading it, an empty table is returned.
     *
     * @param filename - the file to read.
     * @return HighScoreTable.
     */
    public static HighScoresTable loadFromFile(File filename) {
        ObjectInputStream objectInputStream = null;
        HighScoresTable table = null;
        try {
            objectInputStream = new ObjectInputStream(
                    new FileInputStream(filename));

            table = (HighScoresTable) objectInputStream.readObject();
        } catch (FileNotFoundException e) { // Can't find file to open
            System.err.println("Unable to find file: " + filename);
            return null;
        } catch (ClassNotFoundException e) { // The class in the stream is unknown to the JVM
            System.err.println("Unable to find class for object in file: " + filename);
            return null;
        } catch (IOException e) {
            System.err.println("Failed reading object");
            e.printStackTrace(System.err);
            return null;
        } finally {
            try {
                if (objectInputStream != null) {
                    objectInputStream.close();
                }
            } catch (IOException e) {
                System.err.println("Failed closing file: " + filename);
            }
        }
        return table;
    }
}


