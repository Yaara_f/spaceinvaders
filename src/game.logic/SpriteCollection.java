package game.logic;

import biuoop.DrawSurface;

import java.util.List;

/**
 * The Class represent SpriteCollection.
 * SpriteCollection has one field - list of Sprite.
 * The program perform method that are related to SpriteCollection,
 * like: add sprite to the list of the sprites, call timePassed() on all sprites,
 * and draw all the sprites on given surface.
 *
 * @author Yaara Fridchay
 */
public class SpriteCollection {
    private List<Sprite> spriteCo;

    /**
     * Constructor of SpriteCollection.
     *
     * @param sprite - list of sprite
     */
    public SpriteCollection(List<Sprite> sprite) {
        this.spriteCo = sprite;
    }

    /**
     * the method return the list of the sprites of the game.
     *
     * @return - List<Sprite>
     */
    public List<Sprite> getSpriteCollection() {
        return spriteCo;
    }

    /**
     * The method add given sprite to the list of the sprites of the game.
     *
     * @param s - given sprite
     */
    public void addSprite(Sprite s) {
        spriteCo.add(s);
    }

    /**
     * The method remove given sprite from the list of the sprites of the game.
     *
     * @param s - given sprite
     */
    public void removeSprite(Sprite s) {
        spriteCo.remove(s);
    }

    /**
     * The method call the method timePassed() on all sprites.
     *
     * @param dt - the amount of seconds passed since the last call.
     */
    public void notifyAllTimePassed(double dt) {
        if (spriteCo.size() > 0) {
            for (int i = 0; i < spriteCo.size(); i++) {
                spriteCo.get(i).timePassed(dt);
            }
        }
    }

    /**
     * The method draw all the sprites on given surface.
     *
     * @param d - drawSurface
     */
    public void drawAllOn(DrawSurface d) {
        if (spriteCo.size() > 0) {
            for (int i = 0; i < spriteCo.size(); i++) {
                spriteCo.get(i).drawOn(d);
            }
        }
    }

    /**
     * Reset.
     */
    public void reset() {
        spriteCo.clear();
    }
}