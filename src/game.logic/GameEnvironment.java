package game.logic;

import geometry.Collidable;
import geometry.CollisionInfo;
import geometry.Line;
import geometry.Point;

import java.util.List;

/**
 * The Class represent GameEnvironment.
 * GameEnvironment has one field: list of collidable of the game.
 * The program perform method that are related to GameEnvironment,
 * like: add collidable to the list of collidables and check who is the closest collision.
 *
 * @author Yaara Fridchay
 */
public class GameEnvironment {
    private List<Collidable> enviroment;

    /**
     * Constructor of GameEnvironment.
     *
     * @param enviroment - the list of the enviroments.
     */
    public GameEnvironment(List enviroment) {
        this.enviroment = enviroment;
    }

    /**
     * Reset.
     */
    public void reset() {
        this.enviroment.clear();
    }

    /**
     * The method add the given collidable to the environment.
     *
     * @param c - the collidable to add
     */
    public void addCollidable(Collidable c) {
        this.enviroment.add(c);
    }

    /**
     * The method remove the given collidable from the environment.
     *
     * @param c - the collidable to add
     */
    public void removeCollidable(Collidable c) {
        this.enviroment.remove(c);
    }

    /**
     * The method return the list of the collidables.
     *
     * @return List<Collidable> enviroment
     */
    public List<Collidable> getEnviroment() {
        return enviroment;
    }

    // Assume an object moving from line.start() to line.end().
    // If this object will not collide with any of the collidables
    // in this collection, return null. Else, return the information
    // about the closest collision that is going to occur.

    /**
     * The method check if the object hit collidable.
     * If this object will not collide with any of the collidables,
     * in this collection, return null. Else, return the information,
     * about the closest collision that is going to occur.
     *
     * @param trajectory - the line of movement of the object
     * @return the CollisionInfo
     */
    public CollisionInfo getClosestCollision(Line trajectory) {
        // flag if there is a collision
        boolean collision = false;
        //if there isn't any collidable
        if (this.enviroment.size() == 0) {
            return null;
        }
        Point min = new Point(800, 800);
        Point temp = min;
        Collidable c = this.enviroment.get(0);
        // run over all the enviroment
        for (int i = 0; i < this.enviroment.size(); i++) {
            // check the closest point Intersection To Start Of Line
            temp = trajectory.closestIntersectionToStartOfLine(this.enviroment.get(i).getCollisionRectangle());
            if (temp == null) {
                continue;
            }
            // check which of the point more closest to the start of the trajectory
            if (temp.distance(trajectory.start()) <= min.distance(trajectory.start())) {
                collision = true;
                min = temp;
                c = this.enviroment.get(i);
            }
        }
        // if wasn't collision
        if (!collision) {
            return null;
        }
        CollisionInfo result = new CollisionInfo(min, c);
        return result;
    }
}