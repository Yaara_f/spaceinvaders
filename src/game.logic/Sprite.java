package game.logic;

import biuoop.DrawSurface;

/**
 * Interface Sprite represent things that can be draw on the screen.
 */
public interface Sprite {
    /**
     * The method draw the sprite to the screen.
     *
     * @param d - drawSurface
     */
    void drawOn(DrawSurface d);

    /**
     * The method notify the sprite that time has passed.
     *
     * @param dt - the amount of seconds passed since the last call.
     */
    void timePassed(double dt);
}