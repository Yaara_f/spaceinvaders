package game.logic;

import biuoop.DrawSurface;
import geometry.Ball;
import geometry.Block;
import geometry.Point;
import geometry.Velocity;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


/**
 * The Class represent the group of the enemies of the game, it's responsible of the movement of the enemies,
 * the shoot of the enemies and more things.
 */
public class GroupEnemy implements Sprite {
    private List<Block> enemys;
    private GameLevel game;
    private long lastShot;
    private double speed, oldSpeed;
    private boolean isLose;

    /**
     * Instantiates a new Group enemy.
     *
     * @param enemys the enemys
     * @param game   the game
     * @param s      the s
     */
    public GroupEnemy(List<Block> enemys, GameLevel game, Double s) {
        this.enemys = enemys;
        this.lastShot = 0;
        this.game = game;
        this.speed = s;
        this.isLose = false;
        this.oldSpeed = this.speed;
    }

    /**
     * Sets the enemies.
     *
     * @param enemy the enemy
     */
    public void setEnemys(List<Block> enemy) {
        this.enemys = enemy;
    }

    /**
     * Gets size group.
     *
     * @return the size group
     */
    public int getSizeGroup() {
        return this.enemys.size();
    }

    @Override
    public void drawOn(DrawSurface d) {
        for (Block enemy : this.enemys) {
            enemy.drawOn(d);
        }
    }

    @Override
    public void timePassed(double dt) {
        this.shoot();
        this.move(dt);
    }

    /**
     * Find the lowest enemies in th group.
     *
     * @return the list
     */
    public List<Block> findMin() {
        List<Block> enemysMin = new ArrayList<>();
        double xBlock, yBlock;
        for (int i = 0; i < this.enemys.size(); i++) {
            Block temp = this.enemys.get(i);
            xBlock = this.enemys.get(i).getCollisionRectangle().getUpperLeft().getX();
            yBlock = this.enemys.get(i).getCollisionRectangle().getUpperLeft().getY();
            for (int j = 0; j < this.enemys.size(); j++) {
                if (this.enemys.get(j).getCollisionRectangle().getUpperLeft().getX() == xBlock) {
                    if (this.enemys.get(j).getCollisionRectangle().getUpperLeft().getY() > yBlock) {
                        yBlock = this.enemys.get(j).getCollisionRectangle().getUpperLeft().getY();
                        temp = this.enemys.get(j);
                    }
                }
            }
            enemysMin.add(temp);
        }
        return enemysMin;
    }


    /**
     * Shoot.
     */
    public void shoot() {
        if (System.currentTimeMillis() - lastShot > 500) {
            List<Block> mins = findMin();
            Random rand = new Random();
            Block min = mins.get(rand.nextInt(mins.size()));
            Ball shot = this.game.enemyShot(new Point(min.getCollisionRectangle().getBottom().middle().getX(),
                    min.getCollisionRectangle().getBottom().middle().getY() + 1));
            shot.setVelocity(Velocity.fromAngleAndSpeed(180, 200));
            shot.addToGame(this.game);
            this.lastShot = System.currentTimeMillis();
        }
    }


    /**
     * Remove enemy from the enemy group.
     *
     * @param enemy the enemy
     */
    public void remove(Block enemy) {
        this.enemys.remove(enemy);
    }

    /**
     * Move the enemies.
     *
     * @param dt the dt
     */
    public void move(double dt) {
        for (int i = 0; i < this.enemys.size(); i++) {
            if (atEndScreen()) {
                this.speed = -this.speed * 1.1;
                for (Block enemy : this.enemys) {
                    enemy.setUpperLeft(enemy.getCollisionRectangle().getUpperLeft().getX(),
                            enemy.getCollisionRectangle().getUpperLeft().getY() + 10);
                }
                break;
            }
        }
        for (Block enemy : this.enemys) {
            if (enemy.getCollisionRectangle().getUpperLeft().getY() + 30 >= 450) {
                this.isLose = true;
            }
        }
        for (Block enemy : this.enemys) {
            enemy.move(this.speed * dt);
        }
    }

    /**
     * if there is enemy at the end screen.
     *
     * @return the boolean
     */
    public boolean atEndScreen() {

        for (int i = 0; i < this.enemys.size(); i++) {
            if (enemys.get(i).getCollisionRectangle().getUpperLeft().getX() <= 0) {
                return true;
            }
            if (enemys.get(i).getCollisionRectangle().getUpperLeft().getX()
                    + enemys.get(i).getCollisionRectangle().getWidth() >= 800) {
                return true;
            }
        }
        return false;
    }

    /**
     * Is lose boolean.
     *
     * @return the boolean
     */
    public boolean isLose() {
        return this.isLose;
    }

    /**
     * Reset the enemies.
     */
    public void reset() {
        for (Block enemy : this.enemys) {
            enemy.setUpperLeft(enemy.getStartPoint().getX(), enemy.getStartPoint().getY());
        }
        this.isLose = false;
        this.speed = this.oldSpeed;
    }

    /**
     * Gets enamys.
     *
     * @return the enamys
     */
    public List<Block> getEnamys() {
        return this.enemys;
    }
}

