package game.logic;

import geometry.Ball;
import geometry.Block;
import geometry.Paddle;

/**
 * The program represent ScoreTrackingListener that implements the HitListener interface.
 * ScoreTrackingListener responsible to increase the scores according to the hits of the balls in the game.
 * ScoreTrackingListener has one field: Counter that count the scores in the game.
 */
public class ScoreTrackingListener implements HitListener {
    private Counter currentScore;

    /**
     * Constructor of ScoreTrackingListener.
     *
     * @param scoreCounter - the scores in the game.
     */
    public ScoreTrackingListener(Counter scoreCounter) {
        this.currentScore = scoreCounter;
    }

    /**
     * The method responsible to increase the scores when the is an hit event in the game.
     *
     * @param beingHit - Block
     * @param hitter   - Ball
     */
    public void hitEvent(Block beingHit, Ball hitter) {
        if (beingHit.getNumOfHit() == 0) {
            this.currentScore.increase(100);
        }
    }

    @Override
    public void hitEvent(Paddle hitted, Ball ball) {

    }
}