package game.logic;

import java.io.Serializable;

/**
 * The class represent the ScoreInfo that implement Serializable, its has fields of the information of the score,
 * like: the name of the player that reach that score, and the score.
 */
public class ScoreInfo implements Serializable {
    private String name;
    private int score;

    /**
     * Constructor of ScoreInfo.
     *
     * @param name  - name of the player that reach that score.
     * @param score - the score.
     */
    public ScoreInfo(String name, int score) {
        this.name = name;
        this.score = score;
    }

    /**
     * The method return the name of the player that reach the score.
     *
     * @return String.
     */
    public String getName() {
        return this.name;
    }

    /**
     * The method return the score.
     *
     * @return int.
     */
    public int getScore() {
        return this.score;
    }
}
