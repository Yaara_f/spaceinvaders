package game.logic;

/**
 * The method represent the interface HitNotifier.
 */
public interface HitNotifier {

    /**
     * The method add hl as a listener to hit events.
     *
     * @param hl - HitListener
     */
    void addHitListener(HitListener hl);

    /**
     * The method remove hl from the list of listeners to hit events.
     *
     * @param hl - HitListener
     */
    void removeHitListener(HitListener hl);
}