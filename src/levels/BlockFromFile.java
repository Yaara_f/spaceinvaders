package levels;

import game.logic.GameLevel;
import geometry.Block;
import geometry.Point;
import geometry.Rectangle;

import java.awt.Color;
import java.util.TreeMap;

/**
 * The class represent a block that created from file, and implements BlockCreator.
 */
public class BlockFromFile implements BlockCreator {
    private int width, height;
    private int numHits;
    private TreeMap<Integer, ColorsParser> fill;
    private Color frame;
    private GameLevel g;

    /**
     * The method set the height of the block.
     *
     * @param h - height.
     */
    public void setHeight(String h) {
        this.height = Integer.parseInt(h);
    }

    /**
     * The method set the width of the block.
     *
     * @param w - width.
     */
    public void setWidth(String w) {
        this.width = Integer.parseInt(w);
    }

    /**
     * The method set the numHits of the block.
     *
     * @param numH - numHits.
     */
    public void setNumHits(String numH) {
        this.numHits = Integer.parseInt(numH);
    }

    /**
     * The method set the fill of the block.
     *
     * @param f - TreeMap of fill.
     */
    public void setFill(TreeMap<Integer, ColorsParser> f) {
        this.fill = f;
    }

    /**
     * The method set the color frame of the block.
     *
     * @param s - String.
     */
    public void setFrame(String s) {
        ColorsParser c = new ColorsParser();
        this.frame = c.colorFromString(s);
    }

    @Override
    public Block create(int xPos, int yPos) {
        if (this.frame != null) {
            return new Block(new Rectangle(new Point(xPos, yPos), this.width, this.height),
                    numHits, this.fill, this.frame, true);
        } else {
            return new Block(new Rectangle(new Point(xPos, yPos), this.width, this.height),
                    this.numHits, this.fill, this.frame, true);
        }
    }
}
