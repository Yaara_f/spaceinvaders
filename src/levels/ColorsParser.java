package levels;

import javax.imageio.ImageIO;
import java.awt.Color;
import java.awt.Image;

/**
 * The class responsible to convert string to color, and has a fields of Color and Image.
 */

public class ColorsParser {
    private Color color;
    private Image image;

    /**
     * Empty Constructor of ColorParser.
     */
    public ColorsParser() {
    }

    /**
     * Constructor 1 of ColorParser.
     *
     * @param i - image.
     */
    public ColorsParser(Image i) {
        this.image = i;
        this.color = null;
    }

    /**
     * Constructor 2 of ColorParser.
     *
     * @param c - Color.
     */
    public ColorsParser(Color c) {
        this.color = c;
        this.image = null;
    }

    /**
     * The method return the Image.
     *
     * @return Image.
     */
    public Image getImage() {
        return this.image;
    }

    /**
     * The method return the Color.
     *
     * @return Color.
     */
    public Color getColor() {
        return this.color;
    }

    /**
     * The method convert String to Image.
     *
     * @param s - String to convert.
     * @return - Image.
     */
    public java.awt.Image imageFromString(String s) {
        String[] imageArr = s.split("[(|)]");
        Image img = null;
        try {
            img = ImageIO.read(ClassLoader.getSystemClassLoader().getResourceAsStream(imageArr[1]));
        } catch (Exception e) {
            return null;
        }
        return img;
    }

    /**
     * The method convert String to Color.
     *
     * @param s - String to convert.
     * @return Color.
     */
    public java.awt.Color colorFromString(String s) {
        String[] temp = s.split("\\(");
        temp = temp[temp.length - 1].split("\\)");
        s = temp[0];
        // if contains "," it's an RGB color.
        if (s.contains(",")) {
            String[] rgb = s.split(",");
            // create RGB color.
            return new Color(Integer.parseInt(rgb[0]), Integer.parseInt(rgb[1]), Integer.parseInt(rgb[2]));
        } else {
            if (s.equals("black")) {
                return Color.BLACK;
            }
            if (s.equals("blue")) {
                return Color.blue;
            }
            if (s.equals("cyan")) {
                return Color.cyan;
            }
            if (s.equals("gray")) {
                return Color.gray;
            }
            if (s.equals("lightGray")) {
                return Color.lightGray;
            }
            if (s.equals("green")) {
                return Color.green;
            }
            if (s.equals("orange")) {
                return Color.orange;
            }
            if (s.equals("pink")) {
                return Color.pink;
            }
            if (s.equals("red")) {
                return Color.red;
            }
            if (s.equals("white")) {
                return Color.white;
            }
            if (s.equals("yellow")) {
                return Color.yellow;
            }
        }
        return null;
    }
}
