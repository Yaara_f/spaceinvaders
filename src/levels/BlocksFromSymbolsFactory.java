package levels;

import geometry.Block;

import java.util.TreeMap;

/**
 * An object with a method that will get a symbol and create the desired block.
 */
public class BlocksFromSymbolsFactory {

    private TreeMap<String, Integer> spaceWidths;
    private TreeMap<String, BlockCreator> blockCreator;

    /**
     * Constructor of BlocksFromSymbolsFactory.
     *
     * @param space  - TreeMap to the space Symbol.
     * @param blocks - TreeMap to the Blocks.
     */
    public BlocksFromSymbolsFactory(TreeMap<String, Integer> space, TreeMap<String, BlockCreator> blocks) {
        this.spaceWidths = space;
        this.blockCreator = blocks;
    }

    /**
     * The method returns true if 's' is a valid space symbol.
     *
     * @param s - String.
     * @return boolean.
     */
    public boolean isSpaceSymbol(String s) {
        return this.spaceWidths.containsKey(s);
    }

    /**
     * The method returns true if 's' is a valid block symbol.
     *
     * @param s -String.
     * @return boolean.
     */
    public boolean isBlockSymbol(String s) {
        return this.blockCreator.containsKey(s);
    }

    /**
     * The method return a block according to the definitions associated with symbol s.
     * The block will be located at position (xpos, ypos).
     *
     * @param s    - symbol.
     * @param xpos - x Position.
     * @param ypos - y Position.
     * @return Block.
     */
    public Block getBlock(String s, int xpos, int ypos) {
        return this.blockCreator.get(s).create(xpos, ypos);
    }

    /**
     * The method returns the width in pixels associated with the given spacer-symbol.
     *
     * @param s - string.
     * @return - int.
     */
    public int getSpaceWidth(String s) {
        return this.spaceWidths.get(s);
    }
}


