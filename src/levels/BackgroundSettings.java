package levels;

import biuoop.DrawSurface;
import game.logic.Sprite;

import java.awt.Color;
import java.awt.Image;

/**
 * The class BackgroundSettings implements the interface Sprite.
 * it's hold the setting of the background, and draw the background.
 */
public class BackgroundSettings implements Sprite {
    private Image image;
    private Color color;

    /**
     * Constructor 1 of BackgroundSettings.
     *
     * @param img - Image.
     */
    public BackgroundSettings(Image img) {
        this.image = img;
    }

    /**
     * Constructor 2 of BackgroundSettings.
     *
     * @param c - Color.
     */
    public BackgroundSettings(Color c) {
        this.color = c;
    }

    /**
     * The method draw the background on the given drawSurface.
     *
     * @param d - drawSurface
     */
    @Override
    public void drawOn(DrawSurface d) {
        if (this.color != null) {
            d.setColor(this.color);
            d.fillRectangle(0, 0, 800, 600);
        } else {
            d.drawImage(0, 0, this.image);
        }
    }

    /**
     * @param dt - the amount of seconds passed since the last call.
     */
    @Override
    public void timePassed(double dt) {

    }

}
