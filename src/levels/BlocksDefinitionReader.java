package levels;

import java.io.BufferedReader;

import java.util.TreeMap;

/**
 * The class in charge of reading a block-definitions file and returning a BlocksFromSymbolsFactory object.
 */
public class BlocksDefinitionReader {
    /**
     * The method reading a block-definitions file and returning a BlocksFromSymbolsFactory object.
     *
     * @param reader - java.io.Reader.
     * @return BlocksFromSymbolsFactory.
     */
    public static BlocksFromSymbolsFactory fromReader(java.io.Reader reader) {
        // map for the default values.
        TreeMap<String, String> defaultMap = new TreeMap<>();
        // map for the blocks.
        TreeMap<String, String> blockMap;
        // map for the space map.
        TreeMap<String, Integer> spaceMap = new TreeMap<>();
        // map for the BlocksFromSymbolsFactory.
        TreeMap<String, BlockCreator> factoryMap = new TreeMap<>();
        BufferedReader text = new BufferedReader(reader);
        String line, key, value;
        String[] splitLine;
        try {
            // read if the line isn't empty.
            while ((line = text.readLine()) != null) {
                // if it's note, continue.
                if (line.startsWith("#")) {
                    continue;
                } else if (line.startsWith("default")) {
                    splitLine = line.split(" ");
                    // enter the values of the default in the default map.
                    for (int i = 1; i < splitLine.length; i++) {
                        key = splitLine[i].split(":")[0];
                        value = splitLine[i].split(":")[1];
                        defaultMap.put(key, value);
                    }
                } else if (line.startsWith("bdef")) { // start description of block.
                    splitLine = line.split(" ");
                    blockMap = new TreeMap<>();
                    // insert the default values.
                    blockMap.putAll(defaultMap);
                    // insert the values of the block to the map block.
                    for (int i = 1; i < splitLine.length; i++) {
                        key = splitLine[i].split(":")[0];
                        value = splitLine[i].split(":")[1];
                        blockMap.put(key, value);
                    }
                    String symbol = blockMap.get("symbol");
                    // create the map of the fill of the block.
                    TreeMap<Integer, ColorsParser> fill = new TreeMap<>();
                    ColorsParser colorsParser = new ColorsParser();
                    if (blockMap.containsKey("fill")) {
                        for (int j = 1; j <= Integer.parseInt(blockMap.get("hit_points")); j++) {
                            if (blockMap.get("fill").startsWith("color")) { // insert the color fill
                                fill.put(j, new ColorsParser(colorsParser.colorFromString(blockMap.get("fill"))));
                            } else { // insert the image fill.
                                fill.put(j, new ColorsParser(colorsParser.imageFromString(blockMap.get("fill"))));
                            }
                        }
                    }
                    // put the fill in the map with the keys according to hit pints.
                    for (int j = 1; j <= Integer.parseInt(blockMap.get("hit_points")); j++) {
                        if (blockMap.containsKey("fill-" + j)) {
                            if (blockMap.get("fill-" + j).startsWith("color")) {
                                fill.put(j, new ColorsParser(colorsParser.colorFromString(blockMap.get("fill-" + j))));
                            } else {
                                fill.put(j, new ColorsParser(colorsParser.imageFromString(blockMap.get("fill-" + j))));
                            }
                        }
                    }
                    // if some field are missing, return null.
                    if (!blockMap.containsKey("height") || !blockMap.containsKey("width")
                            || !blockMap.containsKey("hit_points") || !blockMap.containsKey("symbol")) {
                        return null;
                    }
                    if (!blockMap.containsKey("fill")) {
                        for (int j = 1; j <= Integer.parseInt(blockMap.get("hit_points")); j++) {
                            if (!blockMap.containsKey("fill-" + j)) {
                                return null;
                            }
                        }
                    }
                    BlockFromFile block = new BlockFromFile();
                    //set the information of the blocks according to the default and block map.
                    block.setHeight(blockMap.get("height"));
                    block.setWidth(blockMap.get("width"));
                    block.setNumHits(blockMap.get("hit_points"));
                    block.setFill(fill);
                    if (blockMap.containsKey("stroke")) {
                        block.setFrame(blockMap.get("stroke"));
                    }
                    factoryMap.put(symbol, block);
                } else if (line.startsWith("sdef")) {       // is it's spacer description
                    splitLine = line.split(" ");
                    key = splitLine[1].split(":")[1];
                    value = splitLine[2].split(":")[1];
                    // insert to spacer map.
                    spaceMap.put(key, Integer.parseInt(value));
                }
            }
        } catch (Exception e) {
            return null;
        } finally {
            try {
                text.close();
            } catch (Exception k) {
                k.printStackTrace();
            }
        }
        return new BlocksFromSymbolsFactory(spaceMap, factoryMap);
    }
}



