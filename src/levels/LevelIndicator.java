package levels;

import biuoop.DrawSurface;
import game.logic.GameLevel;
import game.logic.Sprite;

import java.awt.Color;

/**
 * The program represent LevelIndicator that implements the Sprite interface.
 * levelIndicator has one field: the name of the level.
 * The program perform method that are related to levelIndicator,
 * like: draw the name of the level on given surface and add to game method.
 */
public class LevelIndicator implements Sprite {

    private static final int FONT_SIZE = 20;

    private String nameLevel;

    /**
     * Constructor of LevelIndicator.
     *
     * @param nameLevel - the name of the level.
     */
    public LevelIndicator(String nameLevel) {
        this.nameLevel = nameLevel;
    }

    /**
     * the method draw the name of the level on the given drawSurface.
     *
     * @param d - drawSurface
     */
    @Override
    public void drawOn(DrawSurface d) {
        // the x,y value of the place in the screen that the text will appear on.
        int centerX = 450;
        int centerY = 20;
        d.setColor(Color.BLACK);
        d.drawText(centerX, centerY, "Name Level: " + this.nameLevel, FONT_SIZE);

    }

    /**
     * @param dt - the amount of seconds passed since the last call.
     */
    @Override
    public void timePassed(double dt) {

    }

    /**
     * the method add the levelIndicator to the given game.
     *
     * @param g - Game.
     */
    public void addToGame(GameLevel g) {
        g.addSprite(this);
    }
}