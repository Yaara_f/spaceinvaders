package levels;

import geometry.Block;

/**
 * Interface of Block creator.
 */
public interface BlockCreator {

    /**
     * The method Create a block at the specified location.
     *
     * @param xpos - x position.
     * @param ypos - y position.
     * @return Block.
     */
    Block create(int xpos, int ypos);
}