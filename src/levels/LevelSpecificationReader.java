package levels;

import geometry.Block;
import geometry.Velocity;

import java.awt.Color;
import java.awt.Image;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

/**
 * LevelSpecificationReader is an object with a method,
 * that will get a file name and returns a list of LevelInformation.
 */

public class LevelSpecificationReader {

    /**
     * The method get a file name and returns a list of LevelInformation.
     *
     * @param reader - java.io.Reader.
     * @return list of LevelInformation.
     */
    public static List<LevelInformation> fromReader(java.io.Reader reader) {
        List<LevelInformation> levels = new ArrayList<>();
        BufferedReader lr = new BufferedReader(reader);
        LevelSettings level = new LevelSettings();
        String currentLine;
        String[] partsLine;
        BlocksFromSymbolsFactory factory = null;
        int x = 0, y = 0;
        int rowHeight = 0;
        int startX = 0;
        int counterInfoLevel = 0;
        int flagLevels = 0;
        try {
            while ((currentLine = lr.readLine()) != null) {
                partsLine = currentLine.split(" ");
                // if its empty line or an note, continue and don't read.
                if (currentLine.startsWith("#") || currentLine.length() == 0) {
                    continue;
                } else if (currentLine.equals("START_LEVEL")) { // in level description.
                    // in level.
                    flagLevels++;
                } else if (currentLine.startsWith("level_name")) { // level name.
                    // we get more info of the level.
                    counterInfoLevel++;
                    partsLine = currentLine.split(":");
                    level.setLevelName(partsLine[1]);
                } else if (currentLine.startsWith("ball_velocities")) { // ball Velocities.
                    counterInfoLevel++;
                    partsLine = currentLine.split(":");
                    List<Velocity> vel = new ArrayList<Velocity>();
                    String[] velocities = partsLine[1].split(" ");
                    String[] detail;
                    // create the list of the velocities of the ball.
                    for (int i = 0; i < velocities.length; i++) {
                        detail = velocities[i].split(",");
                        Velocity velocity = Velocity.fromAngleAndSpeed(
                                Double.parseDouble(detail[0]), Double.parseDouble(detail[1]));
                        vel.add(velocity);
                    }
                    level.setVelocities(vel);
                } else if (currentLine.startsWith("background")) { // background
                    counterInfoLevel++;
                    partsLine = currentLine.split(":");
                    Color color;
                    Image img;
                    BackgroundSettings background;
                    ColorsParser tempColorSetter = new ColorsParser();
                    // if the background is color.
                    if (partsLine[1].startsWith("color")) {
                        color = tempColorSetter.colorFromString(partsLine[1]);
                        background = new BackgroundSettings(color);
                    } else { // if the background is image.
                        img = tempColorSetter.imageFromString(partsLine[1]);
                        background = new BackgroundSettings(img);
                    }
                    level.setBackground(background);
                } else if (currentLine.startsWith("paddle_speed")) { //paddle speed.
                    counterInfoLevel++;
                    partsLine = currentLine.split(":");
                    level.setPaddleSpeed(Integer.parseInt(partsLine[1]));
                } else if (currentLine.startsWith("paddle_width")) { // paddle width
                    counterInfoLevel++;
                    partsLine = currentLine.split(":");
                    level.setPaddleWidth(Integer.parseInt(partsLine[1]));
                } else if (currentLine.startsWith("block_definitions")) { // block definitions
                    counterInfoLevel++;
                    partsLine = currentLine.split(":");
                    // load the file of the block definitions.
                    InputStream is = ClassLoader.getSystemClassLoader().getResourceAsStream(partsLine[1]);
                    Reader blocksReader = new InputStreamReader(is);
                    // read the bloch definitions.
                    factory = BlocksDefinitionReader.fromReader(blocksReader);
                    blocksReader.close();
                    if (factory == null) {
                        return null;
                    }
                } else if (currentLine.startsWith("blocks_start_x")) { // blocks_start_x
                    counterInfoLevel++;
                    partsLine = currentLine.split(":");
                    x = Integer.parseInt(partsLine[1]);
                    startX = x;
                } else if (currentLine.startsWith("blocks_start_y")) { //blocks_start_y
                    counterInfoLevel++;
                    partsLine = currentLine.split(":");
                    y = Integer.parseInt(partsLine[1]);
                } else if (currentLine.startsWith("row_height")) { // row_height
                    counterInfoLevel++;
                    partsLine = currentLine.split(":");
                    rowHeight = Integer.parseInt(partsLine[1]);
                } else if (currentLine.startsWith("num_blocks")) { //num_blocks
                    counterInfoLevel++;
                    partsLine = currentLine.split(":");
                    level.setNumBlocks(Integer.parseInt(partsLine[1]));
                } else if (currentLine.startsWith("START_BLOCKS")) { //START_BLOCKS
                    flagLevels++;
                    List<Block> blocks = new ArrayList<Block>();
                    while (true) {
                        currentLine = lr.readLine();
                        if (currentLine.startsWith("END_BLOCKS")) {
                            break;
                        }
                        // if its empty line or an note, continue and don't read.
                        if (currentLine.startsWith("#") || currentLine.length() == 0) {
                            continue;
                        } else { // crate the blocks according to the BlocksFromSymbolsFactory.
                            for (int i = 0, j = 0; i < currentLine.length(); i++) {
                                // if it's a space symbol, do a space according to the spacer.
                                if (factory.isSpaceSymbol(Character.toString(currentLine.charAt(i)))) {
                                    x = x + factory.getSpaceWidth(Character.toString(currentLine.charAt(i)));
                                }
                                // if it's block symbol, create the block.
                                if (factory.isBlockSymbol(Character.toString(currentLine.charAt(i)))) {
                                    Block b = factory.getBlock(Character.toString(currentLine.charAt(i)), x, y);
                                    blocks.add(b);
                                    x = (int) (x + b.getCollisionRectangle().getWidth());
                                }
                            }
                            x = startX;
                            y = y + rowHeight;
                        }
                    }
                    // end a level
                    flagLevels--;
                    level.setBlocks(blocks);
                } else if (currentLine.startsWith("END_LEVEL")) {
                    // end a level
                    flagLevels--;
                    // if there aren't enough info of the level,
                    // or the file stop in the middle of the description od the level.
                    if (counterInfoLevel != 10 || flagLevels != 0) {
                        return null;
                    }
                    levels.add(level);
                    level = new LevelSettings();
                    counterInfoLevel = 0;
                }
            }
        } catch (Exception e) {
            System.out.println("wrong file");
        } finally {
            try {
                lr.close();
            } catch (Exception k) {
                k.printStackTrace();
            }
        }
        return levels;
    }
}



