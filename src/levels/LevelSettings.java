package levels;

import game.logic.Sprite;
import geometry.Block;
import geometry.Velocity;

import java.util.List;

/**
 * The class LevelSettings implement LevelInformation , and responsible to set the fields of level.
 */
public class LevelSettings implements LevelInformation {

    private String levelName;
    private List<Velocity> velocities;
    private int paddleSpeed;
    private int paddleWidth;
    private int numBlocks;
    private List<Block> blocks;
    private BackgroundSettings background;

    /**
     * The method set the name of the level.
     *
     * @param s - the name of the level.
     */
    public void setLevelName(String s) {
        this.levelName = s;
    }

    /**
     * The method set the Velocities of the balls of the level.
     *
     * @param l - List of Velocities.
     */
    public void setVelocities(List<Velocity> l) {
        this.velocities = l;
    }

    /**
     * The method set the speed of the paddle of the level.
     *
     * @param s - the speed of the paddle.
     */
    public void setPaddleSpeed(int s) {
        this.paddleSpeed = s;
    }

    /**
     * The method set the Width of the paddle of the level.
     *
     * @param w - the width of the paddle.
     */
    public void setPaddleWidth(int w) {
        this.paddleWidth = w;
    }

    /**
     * The method set the number of the blocks of the level.
     *
     * @param n - number of balls.
     */
    public void setNumBlocks(int n) {
        this.numBlocks = n;
    }

    /**
     * The method set the blocks of the level.
     *
     * @param l - list of the blocks of th level.
     */
    public void setBlocks(List<Block> l) {
        this.blocks = l;
    }

    /**
     * The method set the background of the level.
     *
     * @param bg - the background.
     */
    public void setBackground(BackgroundSettings bg) {
        this.background = bg;
    }

    @Override
    public int numberOfBalls() {
        return this.velocities.size();
    }

    @Override
    public List<Velocity> initialBallVelocities() {
        return this.velocities;
    }

    @Override
    public int paddleSpeed() {
        return this.paddleSpeed;
    }

    @Override
    public int paddleWidth() {
        return this.paddleWidth;
    }

    @Override
    public String levelName() {
        return this.levelName;
    }

    @Override
    public Sprite getBackground() {
        return this.background;
    }

    @Override
    public List<Block> blocks() {
        return this.blocks;
    }

    @Override
    public int numberOfBlocksToRemove() {
        return this.numBlocks;
    }
}
