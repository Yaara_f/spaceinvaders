package levels;

import game.logic.Sprite;
import geometry.Block;
import geometry.Velocity;

import java.util.List;

/**
 * The program represent the interface levelInformation that include all the information of the current level.
 */
public interface LevelInformation {
    /**
     * The method return the numbers of the balls at the level.
     *
     * @return int
     */
    int numberOfBalls();

    /**
     * The method return the Velocity of the balls of the level.
     *
     * @return List <Velocity>
     */
    List<Velocity> initialBallVelocities();

    /**
     * The method return the speed of the paddle of the level.
     *
     * @return int
     */
    int paddleSpeed();

    /**
     * The method return the the width of the paddle of the level.
     *
     * @return int
     */
    int paddleWidth();

    /**
     * The method return the name of the level.
     *
     * @return String
     */
    String levelName();

    /**
     * The method return the background of the level.
     *
     * @return Sprite
     */
    Sprite getBackground();

    /**
     * The method return the Blocks of the level.
     *
     * @return List <Block>
     */
    List<Block> blocks();

    /**
     * The method return the numbers of the remaining blocks in the level.
     *
     * @return int
     */
    int numberOfBlocksToRemove();
}