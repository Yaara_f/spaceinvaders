package geometry;

import biuoop.DrawSurface;
import game.logic.GameLevel;
import game.logic.HitListener;
import game.logic.HitNotifier;
import game.logic.Sprite;
import levels.ColorsParser;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

/**
 * The program represent Block that implements Collidable, Sprite.
 * Block has some fields: Rectangle block , num of hit = represent how many times the ball hit the block,
 * and String Hits that represent the number that appear on the block (x,1,2).
 * The program perform method that are related to Block,
 * like: draw the block on given surface and method that check if the ball hit the ball.
 *
 * @author Yaara Fridchay
 */
public class Block implements Collidable, Sprite, HitNotifier {


    private List<HitListener> hitListeners = new ArrayList<HitListener>();
    private Rectangle block;
    private int numOfHit;
    private String hits;
    private TreeMap<Integer, ColorsParser> fill;
    private Color frameColor;
    private Point startPoint;
    private boolean isEnemy;

    /**
     * * Constructor of Block.
     *
     * @param rec        - the rectangle of the block.
     * @param numOfHit   - how many times the ball hit the block.
     * @param f          - TreeMap of the fill.
     * @param frameColor - the Color of the frame of the Block.
     * @param isEnemy    - if it's block of enemy.
     */
    public Block(Rectangle rec, int numOfHit, TreeMap<Integer, ColorsParser> f, Color frameColor,
                 boolean isEnemy) {
        this.block = rec;
        this.numOfHit = numOfHit;
        this.fill = f;
        this.frameColor = frameColor;
        this.startPoint = new Point(rec.getUpperLeft().getX(), rec.getUpperLeft().getY());
        this.isEnemy = isEnemy;
    }

    /**
     * The method return the start point before the movement.
     *
     * @return start point
     */
    public Point getStartPoint() {
        return startPoint;
    }

    /**
     * The method return the color of the frame.
     *
     * @return Color. frame color
     */
    public Color getFrameColor() {
        return this.frameColor;
    }

    /**
     * The method return the TreeMap of the fill.
     *
     * @return TreeMap. fill
     */
    public TreeMap<Integer, ColorsParser> getFill() {
        return fill;
    }


    /**
     * The method return how many times the ball hit the block.
     *
     * @return int numOfHit
     */
    public int getNumOfHit() {
        return this.numOfHit;
    }

    /**
     * The method  draw the ball on given surface.
     *
     * @param d - the surface of the game.
     */
    public void drawOn(DrawSurface d) {
        if (this.fill.get(numOfHit).getColor() != null) {
            d.setColor(this.fill.get(this.numOfHit).getColor());
            d.fillRectangle((int) this.block.getUpperLeft().getX(), (int) this.block.getUpperLeft().getY(),
                    (int) this.block.getWidth(), (int) this.block.getHeight());
        } else {
            d.drawImage((int) this.block.getUpperLeft().getX(), (int) this.block.getUpperLeft().getY(),
                    this.fill.get(this.numOfHit).getImage());
        }
        if (this.frameColor != null) {
            d.setColor(this.frameColor);
            d.drawRectangle((int) this.block.getUpperLeft().getX(), (int) this.block.getUpperLeft().getY(),
                    (int) this.block.getWidth(), (int) this.block.getHeight());
        }
    }

    /**
     * the method set the text that appear on the block.
     *
     * @param numberHit - how many times the ball hit the block.
     */
    public void setText(int numberHit) {
        if (this.numOfHit == 2) {
            this.hits = "2";
        }
        if (this.numOfHit == 1) {
            this.hits = "1";
        }
        if (this.numOfHit == 0) {
            this.hits = "x";
        }
    }

    /**
     * The method return the text that appear on the block.
     *
     * @return String - the text.
     */
    public String getText() {
        return this.hits;
    }

    /**
     * @param dt - the amount of seconds passed since the last call.
     */
    public void timePassed(double dt) {

    }

    /**
     * The method return the block.
     *
     * @return block.
     */
    public Rectangle getCollisionRectangle() {
        return this.block;
    }

    /**
     * The method add the block to the list of the Sprites and to the list of the Collidables.
     *
     * @param g - the variable of the game.
     */
    public void addToGame(GameLevel g) {
        g.addSprite(this);
        g.addCollidable(this);
    }

    /**
     * The method remove the block from the game.
     *
     * @param game - GameLevel.
     */
    public void removeFromGame(GameLevel game) {
        game.removeCollidable(this);
        game.removeSprite(this);

    }

    /**
     * The method check where the ball hit the block, and return the  new Velocity of the ball.
     *
     * @param hitter          - the ball that hit the Block.
     * @param collisionPoint  - the point that the ball hit.
     * @param currentVelocity - the current velocity of the ball
     * @return the  new Velocity of the ball after it hit the block.
     */
    public Velocity hit(Ball hitter, Point collisionPoint, Velocity currentVelocity) {
        if (hitter.isEnamyShot() && this.isEnemy) {
            return null;
        }
        // set the lines of the block
        Line[] lines = this.block.linesOfRectangle();
        // the line in place 0 in the array is the top line of the block
        Line top = lines[0];
        // the line in place 1 in the array is the right line of the block
        Line right = lines[1];
        // the line in place 2 in the array is the floor line of the block
        Line floor = lines[2];
        // the line in place 3 in the array is the left line of the block
        Line left = lines[3];
        // update the numbers of hitting the block
        if (this.numOfHit > 0) {
            this.numOfHit--;
        }
        this.setText(this.numOfHit);
        this.notifyHit(hitter);
        //check if the ball hit tha block in the top/floor line of the block and change the velocity of the ball.
        if ((collisionPoint.getX() >= top.start().getX() && collisionPoint.getX() <= top.end().getX())
                || (collisionPoint.getX() >= top.end().getX() && collisionPoint.getX() <= top.start().getX())
                || (collisionPoint.getX() >= floor.start().getX() && collisionPoint.getX() <= floor.end().getX())
                || (collisionPoint.getX() >= floor.end().getX() && collisionPoint.getX() <= floor.start().getX())) {
            if ((collisionPoint.getY() == top.start().getY()) || (collisionPoint.getY() == floor.start().getY())) {
                Velocity newVelocity = new Velocity(currentVelocity.getVelocityDx(),
                        currentVelocity.getVelocityDy() * -1);
                return newVelocity;
            }
        }
        //check if the ball hit tha block in the right/left line of the block and change the velocity of the ball.
        if ((collisionPoint.getY() >= left.start().getY() && collisionPoint.getY() <= left.end().getY())
                || (collisionPoint.getY() >= left.end().getY() && collisionPoint.getY() <= left.start().getY())
                || (collisionPoint.getY() >= right.start().getY() && collisionPoint.getY() <= right.end().getY())
                || (collisionPoint.getY() >= right.end().getY() && collisionPoint.getY() <= right.start().getY())) {
            if ((collisionPoint.getX() == left.start().getX()) || (collisionPoint.getX() == right.start().getX())) {
                Velocity newVelocity = new Velocity(currentVelocity.getVelocityDx() * -1,
                        currentVelocity.getVelocityDy());
                return newVelocity;
            }
        }
        return null;
    }

    @Override
    public void addHitListener(HitListener hl) {
        this.hitListeners.add(hl);
    }

    @Override
    public void removeHitListener(HitListener hl) {
        this.hitListeners.remove(hl);
    }

    /**
     * the Method inform all the hitListener that was a hit event.
     *
     * @param hitter - the Ball that hit.
     */
    private void notifyHit(Ball hitter) {
        // Make a copy of the hitListeners before iterating over them.
        List<HitListener> listeners = new ArrayList<HitListener>(this.hitListeners);
        // Notify all listeners about a hit event:
        for (HitListener hl : listeners) {
            hl.hitEvent(this, hitter);
        }
    }

    /**
     * The method set the upperLeft of the Block.
     *
     * @param x the x
     * @param y the y
     */
    public void setUpperLeft(double x, double y) {
        Point upperLeft = new Point(x, y);
        this.block = new Rectangle(upperLeft, this.block.getWidth(), this.block.getHeight());
    }

    /**
     * Move the block.
     *
     * @param dx the dx
     */
    public void move(double dx) {
        double x = this.block.getUpperLeft().getX() + dx;
        this.setUpperLeft(x, this.block.getUpperLeft().getY());
    }


}

