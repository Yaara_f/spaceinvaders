package geometry;

import biuoop.DrawSurface;
import biuoop.KeyboardSensor;
import game.logic.GameLevel;
import game.logic.HitListener;
import game.logic.HitNotifier;
import game.logic.Sprite;
import levels.ColorsParser;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

/**
 * The Class represent Paddle that implements Sprite, Collidable.
 * Ball has some fields: keyboard sensor, rectangle,
 * the width of the screen, and the speed of the ball of the game.
 * The program perform method that are related to Paddle,
 * like: move the paddle left and right,
 * draw on the paddle and change the velocity of the ball when he hit the paddle.
 *
 * @author Yaara Fridchay
 */
public class Paddle implements Sprite, Collidable, HitNotifier {


    private biuoop.KeyboardSensor keyboard;
    private Block rec;
    private double widthScreen;
    private int speedOfPaddle;
    private Color color;
    private long lastShot;
    private GameLevel gameLevel;
    private List<HitListener> hitListeners;
    private TreeMap<Integer, ColorsParser> fill = new TreeMap<>();

    /**
     * Constructor of Paddle.
     *
     * @param keyboard      - the keyboard sensor
     * @param rec           - the Rectangle
     * @param widthScreen   - the width of the screen of the game.
     * @param speedOfPaddle - the speed of the paddle
     * @param color         - the color of the paddle.
     * @param gameLevel     -the game.
     */
    public Paddle(biuoop.KeyboardSensor keyboard, Block rec, int widthScreen,
                  int speedOfPaddle, Color color, GameLevel gameLevel) {
        this.keyboard = keyboard;
        this.rec = rec;
        this.widthScreen = widthScreen;
        this.speedOfPaddle = speedOfPaddle;
        this.color = color;
        this.lastShot = 0;
        this.gameLevel = gameLevel;
        this.hitListeners = new ArrayList<HitListener>();
        this.fill.put(1, new ColorsParser(Color.orange));

    }

    /**
     * The method move the paddle left.
     *
     * @param dt - the amount of seconds passed since the last call.
     */
    private void moveLeft(double dt) {
        // the point after the movement of the paddle
        Point newP = new Point(this.rec.getCollisionRectangle().getUpperLeft().getX() - this.speedOfPaddle * dt,
                this.rec.getCollisionRectangle().getUpperLeft().getY());
        Block recAfter = new Block(new Rectangle(newP, this.rec.getCollisionRectangle().getWidth(),
                this.rec.getCollisionRectangle().getHeight()), 0, this.fill, Color.black, false);
        this.rec = recAfter;
    }

    /**
     * The method move the paddle right.
     *
     * @param dt - the amount of seconds passed since the last call.
     */
    private void moveRight(double dt) {
        // the point after the movement of the paddle
        Point newP = new Point(this.rec.getCollisionRectangle().getUpperLeft().getX() + this.speedOfPaddle * dt,
                this.rec.getCollisionRectangle().getUpperLeft().getY());
        Block recAfter = new Block(new Rectangle(newP, this.rec.getCollisionRectangle().getWidth(),
                this.rec.getCollisionRectangle().getHeight()), 0, this.fill, Color.black, false);
        this.rec = recAfter;
    }

    /**
     * the method responsible on the movement of the paddle.
     *
     * @param dt - the amount of seconds passed since the last call.
     */
    public void timePassed(double dt) {
        // if the user press the left key
        if (keyboard.isPressed(KeyboardSensor.LEFT_KEY)) {
            // if the paddle have place to move left
            if (this.rec.getCollisionRectangle().getUpperLeft().getX() >= 30) {
                moveLeft(dt);
            }
        }
        // if the user press the right key
        if (keyboard.isPressed(KeyboardSensor.RIGHT_KEY)) {
            // if the paddle have place to move right
            if (this.rec.getCollisionRectangle().getUpperLeft().getX()
                    + this.rec.getCollisionRectangle().getWidth() <= this.widthScreen - 30) {
                moveRight(dt);
            }
        }
        if (keyboard.isPressed(KeyboardSensor.SPACE_KEY)) {
            shoot(this.gameLevel);
        }
    }

    /**
     * The method draw the paddle on given surface.
     *
     * @param d - drawSurface
     */
    public void drawOn(DrawSurface d) {
        d.setColor(this.color);
        d.fillRectangle((int) this.rec.getCollisionRectangle().getUpperLeft().getX(),
                (int) this.rec.getCollisionRectangle().getUpperLeft().getY(),
                (int) this.rec.getCollisionRectangle().getWidth(), (int) this.rec.getCollisionRectangle().getHeight());
        d.setColor(Color.black);
        d.drawRectangle((int) this.rec.getCollisionRectangle().getUpperLeft().getX(),
                (int) this.rec.getCollisionRectangle().getUpperLeft().getY(),
                (int) this.rec.getCollisionRectangle().getWidth(), (int) this.rec.getCollisionRectangle().getHeight());
    }

    /**
     * The method return the Collidable of the paddle.
     *
     * @return - Collidable
     */
    public Rectangle getCollisionRectangle() {
        return this.rec.getCollisionRectangle();
    }

    /**
     * The metod check where the ball hit the paddle and change it velocity,
     * according to the area of hit in the paddle.
     *
     * @param hitter          - the ball that hit the paddle.
     * @param collisionPoint  - the point the ball hit
     * @param currentVelocity - the velocity of the ball
     * @return -  The new velocity after hit.
     */
    public Velocity hit(Ball hitter, Point collisionPoint, Velocity currentVelocity) {
        gameLevel.setPaddleHitted(true);
        this.removeFromGame(gameLevel);
        hitter.removeFromGame(gameLevel);
        return currentVelocity;
    }

    /**
     * the method add the paddle to the game.
     *
     * @param g - the variable of the game
     */
    public void addToGame(GameLevel g) {
        // add the paddle to the list of the sprites
        g.addSprite(this);
        // add the paddle to the list of the collidables
        g.addCollidable(this);
    }

    /**
     * the method remove the paddle from the game.
     *
     * @param game - the variable of the game
     */
    public void removeFromGame(GameLevel game) {
        game.removeCollidable(this);
        game.removeSprite(this);

    }

    /**
     * Shoot.
     *
     * @param game the game
     */
    public void shoot(GameLevel game) {
        if (System.currentTimeMillis() - lastShot > 350) {
            this.lastShot = System.currentTimeMillis();
            Ball shot = game.playerShot(new Point(
                    this.getCollisionRectangle().getTop().middle().getX(),
                    this.getCollisionRectangle().getTop().middle().getY()));
            shot.setVelocity(Velocity.fromAngleAndSpeed(0, 500));
            shot.addToGame(game);
        }
    }


    @Override
    public void addHitListener(HitListener hl) {
        this.rec.addHitListener(hl);
    }

    @Override
    public void removeHitListener(HitListener hl) {
        this.rec.removeHitListener(hl);
    }
}