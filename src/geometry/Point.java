package geometry;

/**
 * The program represent Point.
 * Point has two double numbers, x and y.
 * The program perform method that are related to Point,
 * like: distance between two points and check if two points are equal.
 *
 * @author Yaara Fridchay
 */
public class Point {
    private double x, y;

    /**
     * Constructor of Point.
     *
     * @param x - double number
     * @param y - double number
     */
    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    /**
     * The method calculate the distance between two points.
     *
     * @param other - Point
     * @return d - the distance.
     */
    public double distance(Point other) {
        // The equation of distance between 2 points.
        double d = Math.sqrt(Math.pow(this.getX() - other.getX(), 2) + Math.pow(this.getY() - other.getY(), 2));
        return d;
    }

    /**
     * The method check if two points are equals.
     *
     * @param other - Point
     * @return boolean answer - true: the point are equals, false: the points are not equals.
     */
    public boolean equals(Point other) {
        if ((this.getX() == other.getX()) && (this.getY() == other.getY())) {
            return true;
        }
        return false;
    }

    /**
     * The method return the value of x in the point.
     *
     * @return double x
     */
    public double getX() {
        return this.x;
    }

    /**
     * The method return the value of y in the point.
     *
     * @return double y
     */
    public double getY() {
        return this.y;
    }
}

