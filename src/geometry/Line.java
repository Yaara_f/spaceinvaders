package geometry;
import java.util.List;

/**
 * The program represent Line.
 * Line has two Points, start point of the line and end point of the line.
 * The program perform method that are related to line,
 * like: distance between two points and check if two points are equal.
 *
 * @author Yaara Fridchay
 */

public class Line {
    private Point start;
    private Point end;

    /**
     * Constructor of Line.
     *
     * @param start - start point of the line
     * @param end   - end point of the line
     */
    public Line(Point start, Point end) {
        this.start = start;
        this.end = end;
    }

    /**
     * Constructor of Line.
     *
     * @param x1 - the value of x in the start point of the line.
     * @param y1 - the value of y in the start point of the line.
     * @param x2 - the value of x in the end point of the line.
     * @param y2 - the value of y in the end point of the line.
     */
    public Line(double x1, double y1, double x2, double y2) {
        this.start = new Point(x1, y1);
        this.end = new Point(x2, y2);
    }

    /**
     * The method calculate the length of the line.
     *
     * @return the length of the line
     */
    public double length() {
        // calculate the distance between the start point to the end point.
        return start.distance(end);
    }

    /**
     * The method calculate the middle point of the line.
     *
     * @return the middle point of the line
     */
    public Point middle() {
        double x = (this.end.getX() + this.start.getX()) / 2;
        double y = (this.end.getY() + this.start.getY()) / 2;
        Point middle = new Point(x, y);
        return middle;
    }

    /**
     * The method return the start point of the line.
     *
     * @return the start point of the line
     */
    public Point start() {
        return this.start;
    }

    /**
     * The method return the end point of the line.
     *
     * @return the end point of the line
     */
    public Point end() {
        return this.end;
    }

    /**
     * The method check if the lines intersect.
     * The method returns true if the lines intersect, false otherwise.
     *
     * @param other - other Line
     * @return true if the lines intersect, false otherwise.
     */
    public boolean isIntersecting(Line other) {
        if (this.intersectionWith(other) == null) {
            return false;
        }
        return true;
    }

    /**
     * The method find the intersection point of two lines.
     *
     * @param other - other Line.
     * @return the intersection point
     */
    public Point intersectionWith(Line other) {
        // calculate the incline of the first line.
        double a = ((this.end.getY() - this.start.getY()) / (this.end.getX() - this.start.getX()));
        // calculate b from the straight equation : y=ax+b
        double b = this.start.getY() - (a * this.start.getX());
        // calculate the incline of the second line.
        double aOther = ((other.end.getY() - other.start.getY()) / (other.end.getX() - other.start.getX()));
        // calculate b from the straight equation : y=ax+b
        double bOther = other.start.getY() - (aOther * other.start.getX());
        // if the incline of the lines is equals the lines will never intersection
        if (a == aOther && other.start().getX() == other.end().getX()) {
            return null;
        }
        double intersectionX = 0;
        double intersectionY = 0;
        // if the other line is the type of x=a
        if (other.start().getX() == other.end().getX()) {
            // calculate the value of x of the intersection point;
            intersectionX = other.start().getX();
            // calculate the value of y of the intersection point;
            intersectionY = a * intersectionX + b;
        } else {
            if (this.start().getX() == this.end().getX()) {
                intersectionX = this.start().getX();
                // calculate the value of y of the intersection point;
                intersectionY = aOther * intersectionX + bOther;
            } else {
                // calculate the value of x of the intersection point;
                intersectionX = (bOther - b) / (a - aOther);
                // calculate the value of y of the intersection point;
                intersectionY = a * intersectionX + b;
            }
        }
        /*
            check if the intersection point is on the lines,
            that means that the point is between the start point of the line and the end point of the line.
            if the intersection point is on the lines its return the intersection point,
            else its return null.
         */
        if (((this.start().getX() <= intersectionX) && (intersectionX <= this.end().getX()))
                || ((this.end().getX() <= intersectionX) && (intersectionX <= this.start().getX()))) {
            if (((other.start().getX() <= intersectionX) && (intersectionX <= other.end().getX()))
                    || ((other.end().getX() <= intersectionX) && (intersectionX <= other.start().getX()))) {
                if (((this.start().getY() <= intersectionY) && (intersectionY <= this.end().getY()))
                        || ((this.end().getY() <= intersectionY) && (intersectionY <= this.start().getY()))) {
                    if (((other.start().getY() <= intersectionY) && (intersectionY <= other.end().getY()))
                            || ((other.end().getY() <= intersectionY) && (intersectionY <= other.start().getY()))) {
                        Point intersection = new Point(intersectionX, intersectionY);
                        return intersection;
                    }
                }
            }
        }
        return null;
    }

    /**
     * The method check if the lines are equals.
     * The method return true if the lines are equal, false otherwise.
     *
     * @param other A line
     * @return boolean, true if the lines are equal, false otherwise.
     */
    public boolean equals(Line other) {
        if (((this.start == other.start) && (this.end == other.end))
                || ((this.start == other.end) && (this.end == other.start))) {
            return true;
        }
        return false;
    }

    /**
     * The method check if the line intersect with the rectangle.
     * If this line does not intersect with the rectangle, return null.
     * Otherwise, return the closest intersection point to the start of the line.
     *
     * @param rect - the Rectangle
     * @return Point- the closest intersection point to the start of the line.
     */
    public Point closestIntersectionToStartOfLine(Rectangle rect) {
        List intersectionPoints = rect.intersectionPoints(this);
        // large distance to find the min distance
        double distance = 100;
        double tempDistance = 100;
        // if there isn't intersection points
        if (intersectionPoints.size() == 0) {
            return null;
        }
        // the first intersection point
        Point p = (Point) intersectionPoints.get(0);
        // run over all the intersection points
        for (int i = 0; i < intersectionPoints.size(); i++) {
            tempDistance = this.start.distance((Point) intersectionPoints.get(i));
            // if this intersection point is closest for the start point of the line
            if (tempDistance < distance) {
                distance = tempDistance;
                p = (Point) intersectionPoints.get(i);
            }
        }
        return p;
    }
}