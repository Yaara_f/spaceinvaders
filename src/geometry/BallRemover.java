package geometry;

import game.logic.Counter;
import game.logic.GameLevel;
import game.logic.HitListener;

/**
 * The Class represent BallRemover that implements HitListener interface .
 * BallRemover has some fields: the variable of the game and counter of how many balls remaining in the game.
 * The Class responsible of removing balls from the game.
 *
 * @author Yaara Fridchay
 */
public class BallRemover implements HitListener {
    private GameLevel game;
    private Counter remainingBalls;

    /**
     * Constructor of BallRemover.
     *
     * @param game          - The variable of the game.
     * @param removedBlocks - how many balls remaining in the game.
     */
    public BallRemover(GameLevel game, Counter removedBlocks) {
        this.game = game;
        this.remainingBalls = removedBlocks;
    }

    /**
     * The method remove the given ball from the game.
     *
     * @param beingHit - Block
     * @param hitter   - The ball to be remove.
     */
    public void hitEvent(Block beingHit, Ball hitter) {
        hitter.removeFromGame(game);
    }

    @Override
    public void hitEvent(Paddle hitted, Ball ball) {
        ball.removeFromGame(game);

    }
}
