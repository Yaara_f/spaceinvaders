package geometry;

/**
 * The program represent Velocity.
 * Velocity specifies the change in position on the `x` and the `y` axes.
 * The program perform method that are related to Velocity,
 * like: change angle and speed to dx and dy ,and apply the velocity on point.
 */
public class Velocity {
    private double dx;
    private double dy;

    /**
     * The constructor of Velocity.
     *
     * @param dx - double number
     * @param dy - double number
     */
    public Velocity(double dx, double dy) {
        this.dx = dx;
        this.dy = dy;
    }

    /**
     * The method return the value of dx.
     *
     * @return dx
     */
    public double getVelocityDx() {
        return this.dx;
    }

    /**
     * The method return the value of dy.
     *
     * @return dy
     */
    public double getVelocityDy() {
        return this.dy;
    }

    /**
     * The method convert angle and speed to dx and dy.
     *
     * @param angle - double number
     * @param speed - double number
     * @return new velocity
     */
    public static Velocity fromAngleAndSpeed(double angle, double speed) {
        angle += 180;
        double dx = speed * Math.sin(Math.toRadians(angle));
        double dy = speed * Math.cos(Math.toRadians(angle));
        return new Velocity(dx, dy);
    }

    /**
     * The method take a point with position (x,y) and return a new point,
     * with position (x+dx, y+dy).
     *
     * @param p - point
     * @return new point.
     */
    public Point applyToPoint(Point p) {
        double x = p.getX() + this.dx;
        double y = p.getY() + this.dy;
        Point point = new Point(x, y);
        return point;
    }
}