package geometry;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class represent Rectangle.
 * Rectangle has some fields: upperLeft point, width and height of the rectangle and his color.
 * The program perform method that are related to Rectangle,
 * like: set the lines of the rectangle and check the intersection point with given line.
 *
 * @author Yaara Fridchay
 */
public class Rectangle {
    private Point upperLeft;
    private double width;
    private double height;


    /**
     * Constructor of Rectangle - Create a new rectangle with location and width/height.
     *
     * @param upperLeft - the upperLeft point
     * @param width     - the width of the rectangle
     * @param height    - the height of the rectangle
     */
    public Rectangle(Point upperLeft, double width, double height) {

        this.upperLeft = upperLeft;
        this.width = width;
        this.height = height;
    }

    /**
     * Gets top line.
     *
     * @return the top
     */
    public Line getTop() {
        return  new Line(upperLeft.getX(), upperLeft.getY(),
                upperLeft.getX() + width, upperLeft.getY());

    }

    /**
     * Gets bottom line.
     *
     * @return the bottom
     */
    public Line getBottom() {
        return new Line(upperLeft.getX(), upperLeft.getY() + height,
                upperLeft.getX() + width, upperLeft.getY() + height);
    }

    /**
     * The method set the upperLeft point of the rectangle.
     *
     * @param upperLeftPoint - Point
     */
    public void setUpperLeft(Point upperLeftPoint) {
        this.upperLeft = upperLeftPoint;
    }

    /**
     * The method create an array of lines that include the 4 lines of the rectangle.
     *
     * @return array of lines.
     */
    public Line[] linesOfRectangle() {
        Point corner2 = new Point(this.getUpperLeft().getX() + this.getWidth(), this.getUpperLeft().getY());
        Point corner3 = new Point(this.getUpperLeft().getX() + this.getWidth(),
                this.getUpperLeft().getY() + this.getHeight());
        Point corner4 = new Point(this.getUpperLeft().getX(), this.getUpperLeft().getY() + this.getHeight());
        Line l1 = new Line(this.getUpperLeft(), corner2);
        Line l2 = new Line(corner2, corner3);
        Line l3 = new Line(corner4, corner3);
        Line l4 = new Line(this.getUpperLeft(), corner4);
        Line[] lines = new Line[4];
        lines[0] = l1;
        lines[1] = l2;
        lines[2] = l3;
        lines[3] = l4;
        return lines;
    }

    // Return a (possibly empty) List of intersection points
    // with the specified line.
    //

    /**
     * The method check if there are intersection points with the specified line,
     * and return a list with all the intersection points (possibly empty).
     *
     * @param line - specified line
     * @return list with all the intersection points.
     */
    public java.util.List<Point> intersectionPoints(Line line) {
        //set the lines of the rectangle
        Line[] linesOfRec = linesOfRectangle();
        List<Point> intersectionPoints = new ArrayList<Point>();
        // run over all the lines of the rectangle
        for (int i = 0; i < linesOfRec.length; i++) {
            // if there is an intersection points.
            if (linesOfRec[i].intersectionWith(line) != null) {
                intersectionPoints.add(linesOfRec[i].intersectionWith(line));
            }
        }
        return intersectionPoints;
    }

    /**
     * The method return the width of the rectangle.
     *
     * @return double width
     */
    public double getWidth() {
        return this.width;
    }

    /**
     * The method return the height of the rectangle.
     *
     * @return double height
     */
    public double getHeight() {
        return this.height;
    }

    /**
     * The method returns the upper-left point of the rectangle.
     *
     * @return Point upper left
     */
    public Point getUpperLeft() {
        return this.upperLeft;
    }

    /**
     * The method check if the point is in the Rectangle.
     * if the point in the ractangle- retrun true, else false.
     *
     * @param p - specific Point
     * @return true - if the point is in the Rectangle, else -false.
     */
    public boolean insideRec(Point p) {
        if (p.getX() > this.getUpperLeft().getX() && p.getX() < (this.getUpperLeft().getX() + this.getWidth())) {
            if (p.getY() >= this.getUpperLeft().getY() && p.getY() <= this.getUpperLeft().getY() + this.getHeight()) {
                return true;
            }
        }
        return false;
    }
}
