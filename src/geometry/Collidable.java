package geometry;
/**
 * Interface Collidable represent things that can be collided with.
 */
public interface Collidable {
    /**
     * Return the "collision shape" of the object.
     *
     * @return Rectangle
     */
    Rectangle getCollisionRectangle();

    /**
     * Notify the object that we collided with it at collisionPoint with a given velocity,
     * and return the new velocity that expected after the hit.
     *
     * @param hitter          - The ball that cause the hit.
     * @param collisionPoint  - the point the ball hit
     * @param currentVelocity - the velocity of the ball
     * @return the new velocity of the ball.
     */
    Velocity hit(Ball hitter, Point collisionPoint, Velocity currentVelocity);
}
