package geometry;
/**
 * The Class represent CollisionInfo.
 * CollisionInfo has some fields: the point of the collision and the object that the ball hit.
 * The program perform method that are related to CollisionInfo,
 * like: return the collision point and the collision info.
 *
 * @author Yaara Fridchay
 */

public class CollisionInfo {
    private Point collisionPoint;
    private Collidable collsionObject;

    /**
     * Constructor of CollisionInfo.
     *
     * @param collisionPoint - the point that the ball hit
     * @param collsionObject - the object that the ball hit
     */
    public CollisionInfo(Point collisionPoint, Collidable collsionObject) {
        this.collisionPoint = collisionPoint;
        this.collsionObject = collsionObject;
    }

    /**
     * the method return the point at which the collision occurs.
     *
     * @return - the collision point
     */
    public Point collisionPoint() {
        return this.collisionPoint;
    }

    /**
     * the method return the collidable object involved in the collision.
     *
     * @return - the collision object
     */
    public Collidable collisionObject() {
        return this.collsionObject;
    }
}
