package geometry;

import game.logic.Counter;
import game.logic.GameLevel;
import game.logic.HitListener;

/**
 * The Class represent BlockRemover that implements HitListener interface .
 * BlockRemover has some fields: the variable of the game and counter of how many balls remaining in the game.
 * BlockRemover is in charge of removing blocks from the game,
 * as well as keeping count of the number of blocks that remain.
 */
public class BlockRemover implements HitListener {
    private GameLevel game;
    private Counter remainingBlocks;


    /**
     * Constructor of BlockRemover.
     *
     * @param game          - The variable of the game.
     * @param removedBlocks - how many balls remaining in the game.
     */
    public BlockRemover(GameLevel game, Counter removedBlocks) {
        this.game = game;
        this.remainingBlocks = removedBlocks;
    }

    // Blocks that are hit and reach 0 hit-points should be removed
    // from the game. Remember to remove this listener from the block
    // that is being removed from the game.

    /**
     * The method remove the given block from the game.
     *
     * @param beingHit - the block to remove
     * @param hitter   - Ball
     */
    public void hitEvent(Block beingHit, Ball hitter) {
        //Blocks that are hit and reach 0 hit-points should be removed from the game.
            game.removeBlockFromList(beingHit);
            hitter.removeFromGame(game);
            beingHit.removeFromGame(game);
            beingHit.removeHitListener(this);

            remainingBlocks.decrease(1);

    }

    @Override
    public void hitEvent(Paddle hitted, Ball ball) {
    }
}