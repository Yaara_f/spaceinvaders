package geometry;

import biuoop.DrawSurface;
import game.logic.GameEnvironment;
import game.logic.HitListener;
import game.logic.HitNotifier;
import game.logic.Sprite;
import game.logic.GameLevel;


import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class represent Ball.
 * Ball has some fields: center Point, size of the ball (= radius),
 * color,velocity, and the borders of the frame that the ball draw on.
 * The program perform method that are related to Ball,
 * like: move the ball one step and change the velocity of the ball.
 *
 * @author Yaara Fridchay
 */
public class Ball implements Sprite, HitNotifier {
    private Point center;
    private int size;
    private java.awt.Color color;
    private Velocity v;
    //the borders of the frame that the ball draws on.
    private Point topCorner;
    private Point floorCorner;
    private GameEnvironment gameEnvironment;
    private List<HitListener> hitListeners;
    private GameLevel gameLevel;
    private boolean isEnamyShot;

    /**
     * Constructor of Ball.
     *
     * @param center      - Point center of the ball
     * @param size        - int size of the ball
     * @param color       - color of the ball
     * @param environment the environment
     * @param gameLevel   the game level
     * @param isEnamyShot the is enamy shot
     */
    public Ball(Point center, int size, java.awt.Color color,
                GameEnvironment environment, GameLevel gameLevel, boolean isEnamyShot) {
        this.center = center;
        this.size = size;
        this.color = color;
        this.gameEnvironment = environment;
        this.hitListeners = new ArrayList<>();
        this.isEnamyShot = isEnamyShot;
        this.gameLevel = gameLevel;
    }

    /**
     * Is enamy shot boolean.
     *
     * @return the boolean
     */
    public boolean isEnamyShot() {
        return isEnamyShot;
    }

    /**
     * Notify hit.
     *
     * @param hitted the hitted
     */
    public void notifyHit(Block hitted) {
        List<HitListener> listeners = new ArrayList<HitListener>(this.hitListeners);
        for (HitListener listener : listeners) {
            listener.hitEvent(hitted, this);
        }
    }

    /**
     * Notify all subscribers about hits.
     *
     * @param hitted hitted block
     */
    public void notifyHit(Paddle hitted) {
        List<HitListener> listeners = new ArrayList<HitListener>(this.hitListeners);
        for (HitListener listener : listeners) {
            listener.hitEvent(hitted, this);
        }
    }

    /**
     * The method return the value of x of the center point.
     *
     * @return int x
     */
    public int getX() {
        return (int) this.center.getX();
    }

    /**
     * The method return the value of y of the center point.
     *
     * @return int y
     */
    public int getY() {
        return (int) this.center.getY();
    }

    /**
     * The method return the radius of the ball.
     *
     * @return int size
     */
    public int getSize() {
        return this.size;
    }

    /**
     * The method return the color of the ball.
     *
     * @return color color
     */
    public java.awt.Color getColor() {
        return this.color;
    }

    /**
     * The method draw the ball on a given DrawSurface.
     *
     * @param surface - DrawSurface.
     */
    public void drawOn(DrawSurface surface) {
        surface.setColor(this.color);
        surface.fillCircle((int) this.center.getX(), (int) this.center.getY(), this.size);
        surface.setColor(Color.black);
        surface.drawCircle((int) this.center.getX(), (int) this.center.getY(), this.size);

    }

    /**
     * The method responsible to move the ball.
     *
     * @param dt - the amount of seconds passed since the last call.
     */
    public void timePassed(double dt) {
        this.moveOneStep(dt);
    }

    /**
     * The method responsible to add the ball to the list of the spirits.
     *
     * @param g - the variable of the game
     */
    public void addToGame(GameLevel g) {
        g.addSprite(this);
    }

    /**
     * The method set the velocity of the ball.
     *
     * @param velocity - velocity
     */
    public void setVelocity(Velocity velocity) {
        this.v = velocity;
    }

    /**
     * The method set the velocity of the ball.
     *
     * @param dx - double number
     * @param dy - double number
     */
    public void setVelocity(double dx, double dy) {
        this.v = new Velocity(dx, dy);
    }

    /**
     * The method return the velocity of the ball.
     *
     * @return velocity. velocity
     */
    public Velocity getVelocity() {
        return this.v;
    }

    /**
     * the method set the variable of gameEnvironment.
     *
     * @param gameE - the variable of gameEnvironment.
     */
    public void setGameEnvironment(GameEnvironment gameE) {
        this.gameEnvironment = gameE;
    }

    /**
     * The method set the border of the frame that the ball draws on.
     *
     * @param t - the top corner Point.
     * @param f - the floor corner Point.
     */
    public void setScreen(Point t, Point f) {
        this.topCorner = t;
        this.floorCorner = f;
    }

    /**
     * The method return the to corner Point of the frame that the ball draws on.
     *
     * @return Point top corner
     */
    public Point getTopCorner() {
        return this.topCorner;
    }

    /**
     * The method return the to floor Point of the frame that the ball draws on.
     *
     * @return Point floor corner
     */
    public Point getFloorCorner() {
        return this.floorCorner;
    }

    /**
     * The method remove the ball from the game.
     *
     * @param game - GameLevel
     */
    public void removeFromGame(GameLevel game) {
        game.removeSprite(this);
    }

    /**
     * The method move the ball one step.
     * The method also prevent the ball to going out from the frame,
     * if the ball is at the border of the frame,
     * its change the velocity and the ball move in different direction.
     *
     * @param dt - the amount of seconds passed since the last call.
     */
    public void moveOneStep(double dt) {
        if (this.getVelocity() == null) {
            this.removeFromGame(gameLevel);
            return;
        }
        Velocity dtVe = new Velocity(this.getVelocity().getVelocityDx() * dt,
                this.getVelocity().getVelocityDy() * dt);
        // the point of the place that the ball will be there after the move
        Point afterMove = new Point(this.center.getX() + dtVe.getVelocityDx(),
                this.center.getY() + dtVe.getVelocityDy());
        //the line trajectory is how the ball will move without any obstacles
        Line trajectory = new Line(this.center, afterMove);
        // get the closest collision to the trajectory
        CollisionInfo collisionInfo = this.gameEnvironment.getClosestCollision(trajectory);
        if (collisionInfo == null) {
            this.center = dtVe.applyToPoint(this.center);
        } else {
            //if the ball is in rectangle - It happens when the paddle go over the ball.
            if (collisionInfo.collisionObject().getCollisionRectangle().insideRec(this.center)) {
                this.center = new Point(this.center.getX(), this.center.getY()
                        - collisionInfo.collisionObject().getCollisionRectangle().getHeight());
            } else {
                //create new Velocity after the ball hit. I
                // divided the velocity Dx with 100 to make the Shift of the ball be more smoothly
                Velocity newVe = new Velocity((collisionInfo.collisionPoint().getX() - this.center.getX()
                        - (this.v.getVelocityDx() / 100)),
                        (collisionInfo.collisionPoint().getY() - this.center.getY() - (this.v.getVelocityDy()) / 100));
                this.setVelocity(this.gameEnvironment.getClosestCollision(trajectory).collisionObject().
                        hit(this, this.gameEnvironment.getClosestCollision(trajectory).collisionPoint(), this.v));
                this.center = dtVe.applyToPoint(this.center);
            }
        }
    }

    @Override
    public void addHitListener(HitListener hl) {
        this.hitListeners.add(hl);
    }

    @Override
    public void removeHitListener(HitListener hl) {
        this.hitListeners.remove(hl);

    }
}

