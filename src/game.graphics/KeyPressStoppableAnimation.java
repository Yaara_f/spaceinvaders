package game.graphics;

import biuoop.DrawSurface;
import biuoop.KeyboardSensor;

/**
 * The Class represent KeyPressStoppableAnimation that implements Animation interface .
 * The KeyPressStoppableAnimation has the members: keyboardSensor,a key and animation,
 * and 2 boolean members: stop and alreadyPressed.
 * when the user press the key, the program will stop the animation.
 */
public class KeyPressStoppableAnimation implements Animation {
    private KeyboardSensor keyboard;
    private String key;
    private Animation animation;
    private boolean stop;
    private boolean alreadyPressed;

    /**
     * Constructor of KeyPressStoppableAnimation.
     *
     * @param sensor    - keyboardSensor
     * @param key       - String that represent the key
     * @param animation - the animation that running now.
     */
    public KeyPressStoppableAnimation(KeyboardSensor sensor, String key, Animation animation) {
        this.keyboard = sensor;
        this.key = key;
        this.animation = animation;
        this.stop = false;
        this.alreadyPressed = true;
    }

    /**
     * The method responsible on the graphic frame of the game.
     *
     * @param d  - the drawSurface.
     * @param dt - the amount of seconds passed since the last call.
     */
    @Override
    public void doOneFrame(DrawSurface d, double dt) {
        this.animation.doOneFrame(d, dt);
        if (this.keyboard.isPressed(this.key) && !this.alreadyPressed) {
            this.stop = true;
        } else if (this.keyboard.isPressed(this.key)) {
            this.alreadyPressed = false;
        }
    }

    @Override
    public boolean shouldStop() {
        return this.stop;
    }
}
