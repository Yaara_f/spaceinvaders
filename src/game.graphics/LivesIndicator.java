package game.graphics;

import biuoop.DrawSurface;
import game.logic.Counter;
import game.logic.GameLevel;
import game.logic.Sprite;

import java.awt.Color;

/**
 * The program represent LivesIndicator that implements the Sprite interface.
 * livesIndicator has one field: Counter that count the lives.
 * The program perform method that are related to livesIndicator,
 * like: draw the number of lives on given surface and add to game method.
 */
public class LivesIndicator implements Sprite {

    private static final int FONT_SIZE = 20;

    private Counter livesCounter;

    /**
     * Constructor of LivesIndicator.
     *
     * @param lives - Counter.
     */
    public LivesIndicator(Counter lives) {
        this.livesCounter = lives;
    }

    /**
     * the method draw the name of the level on the given drawSurface.
     *
     * @param d - drawSurface
     */
    @Override
    public void drawOn(DrawSurface d) {
        // the x,y value of the place in the block that the text will appear on.
        int centerX = 50;
        int centerY = 20;
        Integer s = this.livesCounter.getValue();
        d.setColor(Color.BLACK);
        d.drawText(centerX, centerY, "Lives: " + s.toString(), FONT_SIZE);

    }

    /**
     * @param dt - the amount of seconds passed since the last call.
     */
    @Override
    public void timePassed(double dt) {

    }

    /**
     * the method add the livesIndicator to the given game.
     *
     * @param g - Game.
     */
    public void addToGame(GameLevel g) {
        g.addSprite(this);
    }
}
