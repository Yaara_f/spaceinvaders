package game.graphics;

/**
 * Interface of Task that include method that responsible to run the Task.
 *
 * @param <T> Task.
 */
public interface Task<T> {
    /**
     * The method run the task.
     *
     * @return T.
     */
    T run();
}