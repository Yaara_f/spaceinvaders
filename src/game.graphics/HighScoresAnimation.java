package game.graphics;

import biuoop.DrawSurface;
import game.logic.HighScoresTable;
import game.logic.ScoreInfo;

import java.awt.Color;

/**
 * The Class represent HighScoresAnimation that implements Animation interface .
 * The HighScoresAnimation include the graphic of the high scores table.
 */

public class HighScoresAnimation implements Animation {
    private HighScoresTable score;

    /**
     * Constructor of HighScoresAnimation.
     *
     * @param scores - the high score table.
     */
    public HighScoresAnimation(HighScoresTable scores) {
        this.score = scores;
    }

    /**
     * the graphic frame of HighScoresAnimation.
     *
     * @param d  - the drawSurface.
     * @param dt - the amount of seconds passed since the last call.
     */
    @Override
    public void doOneFrame(DrawSurface d, double dt) {
        d.setColor(new Color(73, 20, 77));
        d.fillRectangle(0, 0, d.getWidth(), d.getHeight());
        d.setColor(Color.lightGray);
        d.drawText(30, d.getHeight() / 2 - 250,
                "High Scores: ", 40);
        d.drawText(80, d.getHeight() / 2 - 170,
                "Player Name ", 25);
        d.drawText(440, d.getHeight() / 2 - 170,
                "Scores ", 25);
        d.drawLine(20, 150, 660, 150);
        d.drawLine(20, 151, 660, 151);
        d.setColor(Color.white);
        // draws all the players and there score
        int index = 0;
        for (ScoreInfo scoreInfo : this.score.getHighScores()) {
            d.drawText(90, 180 + index * 40, scoreInfo.getName(), 22);
            d.drawText(450, 180 + index * 40, Integer.toString(scoreInfo.getScore()), 22);
            index++;
        }
    }

    @Override
    public boolean shouldStop() {
        return false;
    }

}
