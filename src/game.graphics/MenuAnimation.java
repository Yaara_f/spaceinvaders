package game.graphics;

import biuoop.DrawSurface;
import biuoop.KeyboardSensor;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

/**
 * The program represent the animation of the menu.
 * Its include methods that responsible on the selection in the menu, on the subMenu, and on the graphic of the menu.
 *
 * @param <T>
 */
public class MenuAnimation<T> implements Menu<T> {
    private KeyboardSensor keyboardSensor;
    private List<Selection> menuSelection;
    private T result;
    private String title;

    /**
     * Constructor of MenuAnimation.
     *
     * @param keyboardSensor - the keyboard
     * @param title          - String
     */
    public MenuAnimation(KeyboardSensor keyboardSensor, String title) {
        this.keyboardSensor = keyboardSensor;
        this.menuSelection = new ArrayList<Selection>();
        result = null;
        this.title = title;
    }

    /**
     * The method responsible to add selections to the menu.
     *
     * @param key       - the key the user should press.
     * @param message   - the message that will appear in th menu
     * @param returnVal - what will run after press the key.
     */
    @Override
    public void addSelection(String key, String message, T returnVal) {
        this.menuSelection.add(new Selection(key, message, returnVal));
    }

    /**
     * The method responsible to add a subMenu to the menu.
     *
     * @param key     -    the key the user should press.
     * @param message - the message that will appear in th menu
     * @param subMenu - the subMenu that should run after the user press the key
     */
    @Override
    public void addSubMenu(String key, String message, Menu<T> subMenu) {
        this.menuSelection.add(new Selection(key, message, subMenu));
    }

    /**
     * return the status of the menu.
     *
     * @return T task.
     */
    @Override
    public T getStatus() {
        return result;
    }

    /**
     * The method create the graphic frame of the menu.
     *
     * @param d  - the drawSurface.
     * @param dt - the amount of seconds passed since the last call.
     */
    @Override
    public void doOneFrame(DrawSurface d, double dt) {
        d.setColor(new Color(226, 254, 255));
        d.fillRectangle(0, 0, d.getWidth(), d.getHeight());
        d.setColor(Color.red);
        d.drawRectangle(10, 10, d.getWidth() - 20, d.getHeight() - 20);
        d.drawRectangle(11, 11, d.getWidth() - 20, d.getHeight() - 20);
        d.setColor(new Color(82, 20, 92));
        d.drawText(150, 100, title, 40);
        for (int i = 0; i < this.menuSelection.size(); i++) {
            d.setColor(new Color(0, 0, 77));
            // print the message of any selection in the menu
            d.drawText(200, 200 + i * 70, this.menuSelection.get(i).getMessage(), 30);
            d.setColor(Color.red);
            // print which key to press to choose the selection
            d.drawText(100, 200 + i * 70, "(" + menuSelection.get(i).getPressedKey() + ")", 42);
            //  which selection task the user press and choose.
            if (this.keyboardSensor.isPressed(menuSelection.get(i).getPressedKey())) {
                this.result = (T) menuSelection.get(i).getReturnVal();
            }
        }

    }

    @Override
    public boolean shouldStop() {
        return this.result != null;
    }
}
