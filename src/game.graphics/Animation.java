package game.graphics;

import biuoop.DrawSurface;

/**
 * Interface Animation represent things that need to appear on the screen.
 */
public interface Animation {
    /**
     * The method is responsible to draw the screen of the game.
     *
     * @param d  - the drawSurface.
     * @param dt - the amount of seconds passed since the last call.
     */
    void doOneFrame(DrawSurface d, double dt);

    /**
     * the method check if the animation should stop.
     *
     * @return - boolean.
     */
    boolean shouldStop();
}