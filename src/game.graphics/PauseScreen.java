package game.graphics;

import biuoop.DrawSurface;
import biuoop.KeyboardSensor;

import java.awt.Color;

/**
 * The class represent the pause message that will print to the user if he press "p".
 * if the user will press space, the message will disappear.
 */
public class PauseScreen implements Animation {
    private KeyboardSensor keyboard;
    private boolean stop;

    /**
     * Constructor of PauseScreen.
     */
    public PauseScreen() {
        this.stop = false;
    }

    /**
     * The method draw the pause message on the given surface.
     * if the user press space, the message disappear.
     *
     * @param d  - the drawSurface
     * @param dt - the amount of seconds passed since the last call.
     */
    public void doOneFrame(DrawSurface d, double dt) {
        d.setColor(Color.lightGray);
        d.fillRectangle(0, 0, d.getWidth(), d.getHeight());
        d.setColor(new Color(0, 0, 77));
        d.drawRectangle(10, 10, d.getWidth() - 20, d.getHeight() - 20);
        d.drawRectangle(11, 11, d.getWidth() - 20, d.getHeight() - 20);
        d.setColor(new Color(0, 0, 77));
        d.drawText(100, d.getHeight() / 2 - 20, "paused -- press space to continue", 32);
    }

    /**
     * the method check if the animation should stop.
     *
     * @return boolean
     */
    public boolean shouldStop() {
        return this.stop;
    }
}