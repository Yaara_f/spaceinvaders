package game.graphics;

import biuoop.DrawSurface;
import game.logic.Counter;

import java.awt.Color;

/**
 * The class represent the winning message that will print to the user if he will win.
 * if the user will press space, the message will disappear.
 */
public class WinningMessage implements Animation {
    private boolean stop;
    private Counter counterScores;

    /**
     * Constructor of WinningMessage.
     *
     * @param counterScores - How many scores the user reach.
     */
    public WinningMessage(Counter counterScores) {
        this.stop = false;
        this.counterScores = counterScores;
    }

    /**
     * The method draw the winning message on the given surface.
     * if the user press space, the message disappear.
     *
     * @param d  - the drawSurface
     * @param dt - the amount of seconds passed since the last call.
     */
    public void doOneFrame(DrawSurface d, double dt) {
        d.setColor(new Color(250, 184, 204));
        d.fillRectangle(0, 0, d.getWidth(), d.getHeight());
        d.setColor(new Color(0, 0, 77));
        d.drawRectangle(10, 10, d.getWidth() - 20, d.getHeight() - 20);
        d.drawRectangle(11, 11, d.getWidth() - 20, d.getHeight() - 20);
        d.setColor(new Color(0, 0, 77));
        d.drawText(120, d.getHeight() / 2 - 100,
                "You Win! ", 40);
        d.drawText(400, d.getHeight() / 2 - 100,
                "Your score is " + this.counterScores.getValue(), 30);
        d.setColor(Color.yellow);
        d.fillCircle(650, 500, 70);
        d.setColor(Color.BLACK);
        d.drawCircle(650, 500, 70);
        d.drawCircle(649, 499, 70);
        d.drawLine(670, 450, 670, 490);
        d.drawLine(671, 450, 671, 490);
        d.drawLine(672, 450, 672, 490);
        d.drawLine(630, 450, 630, 490);
        d.drawLine(631, 450, 631, 490);
        d.drawLine(632, 450, 632, 490);
        d.setColor(Color.red);
        d.fillCircle(650, 515, 25);
        d.setColor(Color.yellow);
        d.fillCircle(650, 505, 25);
    }

    /**
     * the method check if the animation should stop.
     *
     * @return boolean
     */
    public boolean shouldStop() {
        return this.stop;
    }
}