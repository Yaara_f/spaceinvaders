package game.graphics;

/**
 * The class represent Selection of the menu.
 * Selection include: String- the key to press, String - the message to the user of what is the selection,
 * T : the task to do after the user press the key.
 * the class include get methods.
 *
 * @param <T>
 */
public class Selection<T> {
    private String pressedKey;
    private String message;
    private T returnVal;

    /**
     * Constructor of Selection.
     *
     * @param pressedKey - the key to press.
     * @param message    - the message.
     * @param returnVal  - the task to do.
     */
    public Selection(String pressedKey, String message, T returnVal) {
        this.pressedKey = pressedKey;
        this.message = message;
        this.returnVal = returnVal;
    }

    /**
     * The method return the task to do after press the key.
     *
     * @return T.
     */
    public T getReturnVal() {
        return this.returnVal;
    }

    /**
     * The method return the message of the selection.
     *
     * @return String.
     */
    public String getMessage() {
        return this.message;
    }

    /**
     * The method return the pressed key.
     *
     * @return String.
     */
    public String getPressedKey() {
        return this.pressedKey;
    }
}
