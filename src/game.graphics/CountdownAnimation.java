package game.graphics;

import biuoop.DrawSurface;
import biuoop.Sleeper;
import game.logic.SpriteCollection;

import java.awt.Color;

/**
 * The Class represent CountdownAnimation that implements Animation interface .
 * The CountdownAnimation will display the given gameScreen,
 * for numOfSeconds seconds, and on top of them it will show a countdown from countFrom back to 1.
 */
public class CountdownAnimation implements Animation {

    private Sleeper sleeper = new Sleeper();
    private double numOfSeconds;
    private int countFrom;
    private int numCount;
    private SpriteCollection gameScreen;

    /**
     * Constructor of CountdownAnimation.
     *
     * @param numOfSeconds - How much time be on the screen
     * @param countFrom    - from which number start to countdown.
     * @param gameScreen   - the screen of the game.
     */
    public CountdownAnimation(double numOfSeconds, int countFrom, SpriteCollection gameScreen) {
        this.numOfSeconds = numOfSeconds;
        this.countFrom = countFrom;
        this.gameScreen = gameScreen;
        this.numCount = countFrom;
    }

    /**
     * The method print the countdown on the given DrawSurface.
     *
     * @param d  - the drawSurface
     * @param dt - the amount of seconds passed since the last call.
     */
    public void doOneFrame(DrawSurface d, double dt) {
        int n = 700 * (int) this.numOfSeconds;
        this.gameScreen.drawAllOn(d);
        // if the animation should run
        if (!this.shouldStop()) {
            // if the countdown reach 0, print "Go"
            if (this.countFrom == 0) {
                d.setColor(Color.lightGray);
                d.drawText((d.getWidth() / 2) - 60, 400, "GO", 80);
            } else {
                // print the current number in the countdown
                d.setColor(Color.lightGray);
                d.drawText((d.getWidth() / 2) - 30, 400, String.valueOf(this.countFrom), 60);
            }
            if (this.countFrom != this.numCount) {
                this.sleeper.sleepFor(n);
            }
            this.countFrom--;
        }
    }

    /**
     * The method return if the program should stop.
     *
     * @return boolean.
     */
    public boolean shouldStop() {
        if (this.countFrom < -1) {
            return true;
        }
        return false;
    }
}