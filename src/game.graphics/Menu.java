package game.graphics;

/**
 * The Interface represent menu method that should be implement in any class of menu.
 *
 * @param <T>
 */
public interface Menu<T> extends Animation {
    /**
     * The method responsible to add selection to the menu.
     *
     * @param key       - the key the user should press.
     * @param message   - the message that will appear in th menu
     * @param returnVal - what will run after press the key.
     */
    void addSelection(String key, String message, T returnVal);

    /**
     * The method responsible to add subMenu to the menu.
     *
     * @param key     -    the key the user should press.
     * @param message - the message that will appear in th menu
     * @param subMenu - the subMenu that should run after the user press the key
     */
    void addSubMenu(String key, String message, Menu<T> subMenu);

    /**
     * The method return the status of T.
     *
     * @return - the status of T.
     */
    T getStatus();
}