package game.graphics;

import biuoop.DrawSurface;
import game.logic.Counter;
import game.logic.GameLevel;
import game.logic.Sprite;

import java.awt.Color;

/**
 * The program represent ScoreIndicator that implements the Sprite interface.
 * ScoreIndicator has one field: a counter that count the scores in the game.
 * The program perform method that are related to ScoreIndicator,
 * like: draw the number of scores on given surface and add to game method.
 */
public class ScoreIndicator implements Sprite {

    private static final int FONT_SIZE = 20;

    private Counter scoreCounter;

    /**
     * Constructor of ScoreIndicator.
     *
     * @param scores - Counter
     */
    public ScoreIndicator(Counter scores) {
        this.scoreCounter = scores;
    }

    /**
     * the method draw the number of scores on the given drawSurface.
     *
     * @param d - drawSurface
     */
    @Override
    public void drawOn(DrawSurface d) {
        d.setColor(Color.white);
        //draw the rectangle of the scores
        d.fillRectangle(0, 0, 799, 30);
        d.setColor(Color.BLACK);
        //draw the frame of the block
        d.drawRectangle(0, 0, 799, 30);
        d.setColor(Color.white);
        // the x,y value of the place in the block that the text will appear on.
        int centerX = 300;
        int centerY = 20;
        Integer s = this.scoreCounter.getValue();
        d.setColor(Color.BLACK);
        d.drawText(centerX, centerY, "Score: " + s.toString(), FONT_SIZE);

    }

    /**
     * @param dt - the amount of seconds passed since the last call.
     */
    @Override
    public void timePassed(double dt) {

    }

    /**
     * the method add the ScoreIndicator to the given game.
     *
     * @param g - Game.
     */
    public void addToGame(GameLevel g) {
        g.addSprite(this);
    }
}
