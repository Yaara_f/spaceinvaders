package game.graphics;

import biuoop.DrawSurface;
import biuoop.GUI;
import biuoop.Sleeper;

/**
 * The Class represent AnimationRunner.
 * AnimationRunner has some fields: the GUI of the game, and int of how many frames there are per second.
 * The Class responsible of running animation things.
 *
 * @author Yaara Fridchay
 */
public class AnimationRunner {
    private GUI gui;
    private double framesPerSecond;

    /**
     * Constructor of AnimationRunner.
     *
     * @param g               - the GUI of the game
     * @param framesPerSecond - how many frames there are per second
     */
    public AnimationRunner(GUI g, double framesPerSecond) {
        this.gui = g;
        this.framesPerSecond = framesPerSecond;
    }

    /**
     * The method return the GUI.
     *
     * @return - GUI of the game.
     */
    public GUI getGui() {
        return this.gui;
    }

    /**
     * The method running to animation she get.
     *
     * @param animation - animation to be run.
     */
    public void run(Animation animation) {
        Sleeper sleeper = new Sleeper();
        int millisecondsPerFrame = 1000 / (int) this.framesPerSecond;
        //the amount of seconds passed since the last call
        double dt = 1.0 / this.framesPerSecond;
        // if the animation should run
        while (!animation.shouldStop()) {
            long startTime = System.currentTimeMillis(); // timing
            DrawSurface d = this.gui.getDrawSurface();
            // draw the animation
            animation.doOneFrame(d, dt);
            gui.show(d);
            long usedTime = System.currentTimeMillis() - startTime;
            long milliSecondLeftToSleep = millisecondsPerFrame - usedTime;
            if (milliSecondLeftToSleep > 0) {
                sleeper.sleepFor(milliSecondLeftToSleep);
            }
        }
    }
}