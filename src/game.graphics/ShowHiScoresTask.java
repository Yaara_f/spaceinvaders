package game.graphics;

/**
 * The class represent ShowHiScoresTask that show the highscore.
 */
public class ShowHiScoresTask implements Task<Void> {
    private AnimationRunner runner;
    private Animation highScoresAnimation;

    /**
     * Constructor f ShowHiScoresTask.
     *
     * @param runner              - animationRunner.
     * @param highScoresAnimation - the Animation of the highScore
     */
    public ShowHiScoresTask(AnimationRunner runner, Animation highScoresAnimation) {
        this.runner = runner;
        this.highScoresAnimation = highScoresAnimation;
    }

    /**
     * The method run the animation of the highScore.
     *
     * @return null.
     */
    public Void run() {
        this.runner.run(this.highScoresAnimation);
        return null;
    }
}